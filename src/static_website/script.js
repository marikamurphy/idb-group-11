window.onload = populate_table;

const ROWS = 5;
const COLS = 5;

// Used to index into table
const NAME_MAP = {
    'Marika Murphy': 0,
    'Brandon0329': 1,
    'Akash Kwatra': 2,
    'zmo617': 3,
    'Maher Rahman': 4
};

const USERNAME_MAP = {
    'marikamurphy': 0,
    'Brandon0329': 1,
    'akashkw': 2,
    'zmo617': 3,
    'MaherRahman': 4 
}

function populate_table() {
    var total_commits = 0;
    var total_issues = 0;

    for(var c = 0; c < COLS; c++) {
        document.getElementById(`td${0}${c}`).innerHTML = 'Developer';
        //document.getElementById(`td${1}${c}`).innerHTML = 'Hello World';
    }

    fetch('https://gitlab.com/api/v4/projects/21344288/repository/contributors')
    .then(response => response.json())
    .then(json => {
        for(var contributor of json) {
            var name = contributor.name;
            var commits = contributor.commits;
            document.getElementById(`td${3}${NAME_MAP[name]}`).innerHTML = commits;
            total_commits += commits;
        }
        document.getElementById('total_commits').innerHTML = 'Total Commits: ' + total_commits;
    })

    var issues_jsons =  [];
    for(var name in USERNAME_MAP)
        issues_jsons.push(fetch(`https://gitlab.com/api/v4/projects/21344288/issues_statistics?author_username=${name}`));

    Promise.all(issues_jsons)
    .then(promises => Promise.all(promises.map(r => r.json())))
    .then(jsons => {
        for(var i = 0; i < jsons.length; i++) {
            var issues = jsons[i].statistics.counts.all;
            document.getElementById(`td${4}${i}`).innerHTML = issues;
            total_issues += issues;
        }
        document.getElementById('total_issues').innerHTML = 'Total Issues: ' + total_issues;
    })
    .then(_ => {
        // Fill empty spaces in table with N/A
        for(var r = 0; r < ROWS; r++) {
            for(var c = 0; c < COLS; c++) {
                if(!document.getElementById(`td${r}${c}`).innerHTML)
                    document.getElementById(`td${r}${c}`).innerHTML = 0;
            }
        }
    })    
}