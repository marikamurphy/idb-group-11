Steps to run the program: TODO: create a docker image

1. create a virtual python environment so you don't mess up your local installation of python
    - You could probably skip this
    - I recommend virtualenv
    - https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/
    - if you name your virtual env 'venv' it's in the .gitignore already

2. install the following requirements into the virtual enviroment
    - source venv/bin/activate
    - pip install -r requirements.txt
    - if you install additional requirements, update using:
    - pip freeze > requirements.txt

3. put your api keys in a file called api_key.txt in this folder in the following format:
- nasa=<api_key>

4. to run (on linux):
    - python api.py
