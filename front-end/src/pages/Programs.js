import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import {
  Table,
  TableCell,
  TableRow,
  TableContainer,
  TableFooter,
  TablePagination,
  CircularProgress,
  FormControl,
  Button,
  TableBody,
} from "@material-ui/core";
import TablePaginationActions from "../components/TablePaginationActions";
import EnhancedTableHead from "../components/EnhancedTableHead";
import CustomRow from "../components/CustomRow";
import { PRO_GRID_HEAD } from "../components/Constants";
import { Grid_style } from "../components/Styles";
import SearchBar from "../components/SearchBar";
import proPlaceHolder from "../assets/placeholders/proPlaceHolder.png";
import axios from "axios";

//a table of relief programs
const Programs = () => {
  const classes = Grid_style();
  const totalPrograms = 20217;
  const rowsPerPage = 10;
  const [numPros, setNumPros] = useState(totalPrograms);

  //rows: current instances
  const [rows, setRows] = useState([]);
  const [loading, setLoading] = useState(false);
  //reset search
  const [reset, setReset] = useState(false);
  //sort
  const [order, setOrder] = React.useState("asc");
  const [orderBy, setOrderBy] = React.useState("placeholder");
  const [request, setRequest] = useState("");
  //pagination
  const [page, setPage] = React.useState(0);
  //for testing
  const [test, setTest] = useState(false);
  const history = useHistory();

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === "asc";
    //if already sorting on something, reset start
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };

  const handleClick = (event, name) => {
    var idd = encodeURIComponent(name);
    history.push(`/programs/${idd}`);
  };

  //if arguments change, get result from backend
  const [search, setSearch] = useState("");
  useEffect(() => {
    setLoading(true);
    var req = "";
    if (search !== "") {
      req += "search=" + search;
    }
    if (orderBy !== "placeholder") {
      req += "&order=" + orderBy;
    }
    if (order !== "asc") {
      req += " desc";
    }
    axios
      .get(`/api/programs?${req}`)
      .then((res) => {
        let data = res.data;
        var x, item, image;
        var arr = [];
        for (x in data) {
          item = data[x];
          if (item.Program && item.Program.name !== "") {
            image =
              !item.Program.image_url || item.Program.image_url === ""
                ? proPlaceHolder
                : item.Program.image_url;
            arr.push({
              programID: item.Program.id,
              img_url: image,
              type: item.Program.type,
              name: item.Program.name,
              budget: item.Program.budget,
              counties: item.Program.counties[0],
              disaster: item.Program.disaster.name,
            });
          } else {
            setNumPros(item);
          }
        }
        setRows(arr);
        setRequest(req);
        setLoading(false);
      })
      .catch(() => {
        setTest(true);
      });
  }, [search, order, orderBy]);

  //if page changes, load
  useEffect(() => {
    setLoading(true);
    axios
      .get(`/api/programs?${request}&offset=${page * 10}`)
      .then((res) => {
        let data = res.data;
        var x, item, image;
        var arr = [];
        for (x in data) {
          item = data[x];
          if (item.Program && item.Program.name !== "") {
            image =
              !item.Program.image_url || item.Program.image_url === ""
                ? proPlaceHolder
                : item.Program.image_url;
            arr.push({
              programID: item.Program.id,
              img_url: image,
              type: item.Program.type,
              name: item.Program.name,
              budget: item.Program.budget,
              counties: item.Program.counties[0],
              disaster: item.Program.disaster.name,
            });
          }
        }
        setRows(arr);
        setLoading(false);
      })
      .catch(() => {
        setTest(true);
      });
  }, [page]);

  useEffect(() => {
    if (reset === true) {
      setSearch("");
      setOrderBy("placeholder");

      setRequest("");
      setNumPros(totalPrograms);
      setReset(false);
    }
  }, [reset]);

  if (loading) {
    return <CircularProgress />;
  }
  if (test === true) {
    return (
      <div className={classes.root}>
        <h2>Server down.</h2>
      </div>
    );
  }
  return (
    <div className={classes.root}>
      <h1>Relief Programs</h1>
      <p>Click the button to search.</p>
      <SearchBar setSearch={setSearch} modelType="programs" />
      <FormControl className={classes.formControl}>
        <Button
          onClick={(e) => {
            e.preventDefault();
            setReset(true);
          }}
          className={classes.button}
          type="submit"
        >
          Reset search
        </Button>
      </FormControl>
      <TableContainer>
        <Table
          className={classes.table}
          aria-labelledby="tableTitle"
          aria-label="enhanced table"
        >
          <EnhancedTableHead
            classes={classes}
            order={order}
            orderBy={orderBy}
            onRequestSort={handleRequestSort}
            rowCount={rows.length}
            headCells={PRO_GRID_HEAD}
          />
          <TableBody>
            {rows.map((row, index) => (
              <CustomRow row={row} instanceType="programs" search={search} />
            ))}
          </TableBody>
          <TableFooter>
            <TableRow>
              <TablePagination
                rowsPerPageOptions={10}
                colSpan={3}
                count={numPros}
                rowsPerPage={rowsPerPage}
                page={page}
                onChangePage={handleChangePage}
                ActionsComponent={TablePaginationActions}
              />
            </TableRow>
          </TableFooter>
        </Table>
      </TableContainer>
    </div>
  );
};

export default Programs;
