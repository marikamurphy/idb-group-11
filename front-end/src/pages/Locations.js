import React, { useState, useEffect } from "react";
import {
  Table,
  TableContainer,
  TableFooter,
  TablePagination,
  Button,
  TableBody,
  TableRow,
  CircularProgress,
  FormControl,
} from "@material-ui/core";
import countyPlaceHolder from "../assets/placeholders/countyPlaceHolder.png";
import TablePaginationActions from "../components/TablePaginationActions";
import EnhancedTableHead from "../components/EnhancedTableHead";
import Filter from "../components/Filter";
import SearchBar from "../components/SearchBar";
import CustomRow from "../components/CustomRow";
import { STATES, COUNTY_GRID_HEAD } from "../components/Constants";
import axios from "axios";
import { Grid_style } from "../components/Styles";

//show a table of counties
const Locations = () => {
  const classes = Grid_style();
  const totalCounties = 3008;
  const rowsPerPage = 10;
  const [numCounties, setNumCounties] = useState(totalCounties);
  //rows: current instances
  const [rows, setRows] = useState([]);
  const [loading, setLoading] = useState(false);
  const [reset, setReset] = useState(false);
  const [search, setSearch] = useState("");
  //for testing
  const [test, setTest] = useState(false);
  //sort
  const [order, setOrder] = React.useState("asc");
  const [orderBy, setOrderBy] = React.useState("placeholder");
  const [request, setRequest] = useState("");
  const [state, setState] = useState("");
  //pagination
  const [page, setPage] = React.useState(0);
  //for testing

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === "asc";
    //if already sorting on something, reset start
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };

  const handleStateChange = (event) => {
    setState(event.target.value);
  };

  //if reset: reset search
  useEffect(() => {
    if (reset === true) {
      setSearch("");
      setOrderBy("placeholder");
      setState("");

      setRequest("");
      setNumCounties(totalCounties);
      setReset(false);
    }
  }, [reset]);

  //if arguments change, load the result from backend
  useEffect(() => {
    setLoading(true);
    var req = "";
    if (search !== "") {
      req += "search=" + search;
    }
    if (orderBy !== "placeholder") {
      req += "&order=" + orderBy;
    }
    if (order !== "asc") {
      req += "%20desc";
    }
    if (state !== "") {
      req += "&state=" + state;
    }
    axios
      .get(`/api/counties?${req}`)
      .then((res) => {
        let data = res.data;
        var x, item, image;
        var arr = [];
        for (x in data) {
          item = data[x];
          if (item.County) {
            image =
              !item.County.image_url || item.County.image_url === ""
                ? countyPlaceHolder
                : item.County.image_url;
            arr.push({
              state2: item.County.state,
              name: item.County.name,
              population: item.County.population,
              longitude: parseFloat(item.County.longitude),
              latitude: parseFloat(item.County.latitude),
              img_url: image,
            });
          } else {
            setNumCounties(item);
          }
        }
        setRows(arr);
        setRequest(req);
        setLoading(false);
      })
      .catch((err) => {
        setTest(true);
      });
  }, [search, order, orderBy, state]);

  //if page change, request backend
  useEffect(() => {
    setLoading(true);
    axios
      .get(`/api/counties?${request}&offset=${page * 10}`)
      .then((res) => {
        let data = res.data;
        var x, item, image;
        var arr = [];
        for (x in data) {
          item = data[x];
          if (item.County) {
            image =
              !item.County.image_url || item.County.image_url === ""
                ? countyPlaceHolder
                : item.County.image_url;
            arr.push({
              state: item.County.state,
              name: item.County.name,
              population: item.County.population,
              longitude: parseFloat(item.County.longitude),
              latitude: parseFloat(item.County.latitude),
              img_url: image,
            });
          }
        }
        setRows(arr);
        setLoading(false);
      })
      .catch(function (error) {
        setTest(true);
      });
  }, [page]);

  if (test === true) {
    return (
      <div className={classes.root}>
        <h2>Server down.</h2>
      </div>
    );
  }
  if (loading) {
    return <CircularProgress />;
  }
  return (
    <div className={classes.root}>
      <h1>Counties</h1>
      <p>Click the button to search.</p>
      <SearchBar setSearch={setSearch} modelType="counties" />
      <FormControl className={classes.formControl}>
        <Button
          onClick={(e) => {
            e.preventDefault();
            setReset(true);
          }}
          className={classes.button}
          type="submit"
        >
          Reset search
        </Button>
      </FormControl>
      <Filter
        filterName="state"
        selectList={STATES}
        handleChange={handleStateChange}
        filterVar={state}
      />
      <TableContainer>
        <Table
          className={classes.table}
          aria-labelledby="tableTitle"
          aria-label="enhanced table"
        >
          <EnhancedTableHead
            classes={classes}
            order={order}
            orderBy={orderBy}
            onRequestSort={handleRequestSort}
            rowCount={rows.length}
            headCells={COUNTY_GRID_HEAD}
          />
          <TableBody>
            {rows.map((row, index) => (
              <CustomRow row={row} instanceType="counties" search={search} />
            ))}
          </TableBody>
          <TableFooter>
            <TableRow>
              <TablePagination
                rowsPerPageOptions={[10]}
                colSpan={3}
                count={numCounties}
                rowsPerPage={rowsPerPage}
                page={page}
                onChangePage={handleChangePage}
                ActionsComponent={TablePaginationActions}
              />
            </TableRow>
          </TableFooter>
        </Table>
      </TableContainer>
    </div>
  );
};
export default Locations;
