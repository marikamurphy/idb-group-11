import React, { useState, useEffect } from "react";
import { CircularProgress, Paper, Grid } from "@material-ui/core";
import DisasterListTb from "../components/DisasterListTb";
import ListTb from "../components/ListTb";
import proPlaceHolder from "../assets/placeholders/proPlaceHolder.png";
import { makeStyles } from "@material-ui/core/styles";
import axios from "axios";
import { Instance_style } from "../components/Styles";
import { STATES_MAP } from "../components/Constants";

//a program instance has a list of disasters
// and a list of associated counties
//media: map and image
const ProgramInstance = (props) => {
  const classes = Instance_style();
  const [loading, setLoading] = useState(false);
  const [instanceInfo, setInstanceInfo] = useState({});
  const [disaster, setDisaster] = useState([]);
  const [locations, setLocations] = useState([]);
  const [sta, setSta] = useState("");
  const id = props.match.params.id;
  const [test, setTest] = useState(false);

  //load this instance
  useEffect(() => {
    setLoading(true);
    axios
      .get(`/api/programs?name=${id}`)
      .then((res) => {
        let cs = res.data[0].Program.counties.map((item, i) => {
          const instance = {
            name: item,
            img_url: res.data[0].Program.county_image_urls[i],
          };
          return instance;
        });
        let disObj = res.data[0].Program.disaster;
        var dis = {
          fema_id: disObj.fema_id,
          name: disObj.name,
          img_url: res.data[0].Program.disaster_image_urls,
        };

        var stateStr = res.data[0].Program.counties[0].split(" ");
        var curState = stateStr[stateStr.length - 1];
        setInstanceInfo(res.data[0].Program);
        setLocations(cs);
        setDisaster([dis]);
        setSta(STATES_MAP[curState]);
        setLoading(false);
      })
      .catch(() => {
        setTest(true);
      });
  }, []);
  if (test === true) {
    return (
      <div className={classes.root}>
        <h2>Server down.</h2>
      </div>
    );
  }
  if (loading) {
    return <CircularProgress />;
  }
  //use the placeholder image if this
  //program doesn't have an image
  var image =
    !instanceInfo.image_url || instanceInfo.image_url === ""
      ? proPlaceHolder
      : instanceInfo.image_url;

  //for map
  var site =
    "https://maps.google.com/maps?width=100%25&height=600&hl=en&q=" +
    encodeURIComponent(sta) +
    "+(my)&t=&amp;z=14&ie=UTF8&iwloc=B&output=embed";
  return (
    <div className={classes.root}>
      <h1> {instanceInfo.name}</h1>
      <Paper className={classes.paper}>
        <Grid container spacing={4} elevation={0}>
          <Grid item xs={6}>
            <img
              className={classes.img}
              src={image}
              alt={instanceInfo.name}
              width="500"
            />
          </Grid>

          <Grid item xs={6}>
            <iframe
              width="500"
              height="400"
              frameBorder="0"
              scrolling="no"
              marginHeight="0"
              marginWidth="0"
              src={site}
              title="mapForLocation"
            ></iframe>
          </Grid>
        </Grid>
      </Paper>
      <p className={classes.attributes}>
        Budget: {instanceInfo.budget}
        <br />
        Type: {instanceInfo.type}
        <br />
        Program ID: {instanceInfo.id}
        <br />
      </p>
      <h3>Disasters</h3>
      <DisasterListTb items={disaster} search={""} />
      <br />
      <h3>Counties Associated</h3>
      <ListTb items={locations} instanceType={"counties"} />
    </div>
  );
};

export default ProgramInstance;
