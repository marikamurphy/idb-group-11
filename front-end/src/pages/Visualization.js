import React, { useState, useEffect } from "react";
import {
  ProgramsChart,
  DisastersChart,
} from "../components/visual/DisasterVis";
import CountyVis from "../components/visual/CountyVis";
import {
  WatersChart,
  ImpactsChart,
  FishesChart,
} from "../components/visual/OceanVis";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  root: {
    background: "#D2691E",
    color: "#DCDCDC",
    margin: "auto",
    padding: "150px",
    textAlign: "center",
  },
  section: {
    marginTop: "100px",
  },
});

function Visualization() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <h2>Disaster Info</h2>
      <ProgramsChart />
      <DisastersChart />
      <h5 style={{ textAlign: "center" }}>Population per County</h5>
      <CountyVis />
      <h2 className={classes.section}>Conserve Ocean</h2>
      <WatersChart />
      <h5 style={{ textAlign: "center" }}>
        Number of Species per Endangered Status
      </h5>
      <FishesChart />
      <ImpactsChart />
    </div>
  );
}

export default Visualization;
