import React, { useState, useEffect } from "react";
import axios from "axios";
import {
  Grid,
  FormControl,
  Table,
  TableContainer,
  TableFooter,
  TablePagination,
  TableRow,
  Button,
  CircularProgress,
} from "@material-ui/core";
import TablePaginationActions from "../components/TablePaginationActions";
import EnhancedTableHead from "../components/EnhancedTableHead";
import DisasterCard from "../components/DisasterCard";
import Filter from "../components/Filter";
import SearchBar from "../components/SearchBar";
import {
  DISASTER_TYPES,
  SEASONS,
  DIS_GRID_HEAD,
} from "../components/Constants";
import { Grid_style } from "../components/Styles";

// Display a grid of all available species
const Disasters = () => {
  const classes = Grid_style();
  const totalDisasters = 3642;
  const rowsPerPage = 9;
  //for testing
  const [test, setTest] = useState(false);
  //rows are the current instances
  const [rows, setRows] = useState([]);
  const [request, setRequest] = useState("");
  //total number of results
  const [numDisasters, setNumDisasters] = useState(totalDisasters);
  const [loading, setLoading] = useState(false);
  const [reset, setReset] = useState(false);
  //pagination
  const [page, setPage] = React.useState(0);
  //sort
  const [order, setOrder] = React.useState("asc");
  const [orderBy, setOrderBy] = React.useState("placeholder");
  //filters: filter by season and/or type
  const [season, setSeason] = useState("");
  const [filterType, setFilterType] = useState("");
  //searching
  const [search, setSearch] = useState("");

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };
  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === "asc";
    //if already sorting on something, reset start
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };
  const handleSeasonChange = (event) => {
    setSeason(event.target.value);
  };
  const handleTypeChange = (event) => {
    setFilterType(event.target.value);
  };

  //if arguments chanegd, request backend
  useEffect(() => {
    setLoading(true);
    var req = "";
    if (search !== "") {
      req += "search=" + search;
    }
    if (orderBy !== "placeholder") {
      req += "&order=" + orderBy;
    }
    if (order !== "asc") {
      req += " desc";
    }
    if (season !== "") {
      req += "&season=" + season;
    }
    if (filterType !== "") {
      req += "&disaster_type=" + filterType;
    }
    axios
      .get(`/api/disasters?${req}&limit=9`)
      .then((res) => {
        let data = res.data;
        var x, item;
        var arr = [];
        for (x in data) {
          item = data[x];
          if (item.Disaster) {
            arr.push({
              disasterNumber: item.Disaster.fema_id,
              img: item.Disaster.image_url,
              type: item.Disaster.disaster_type,
              name: item.Disaster.name,
              date: item.Disaster.date,
              cost: item.Disaster.cost,
              season: item.Disaster.season,
              numPrograms: item.Disaster.programs.length,
            });
          } else {
            setNumDisasters(item);
          }
        }
        setRows(arr);
        setRequest(req);
        if (page !== 0) {
          setPage(0);
        }
        setLoading(false);
      })
      .catch(() => {
        setTest(true);
      });
  }, [search, season, filterType, order, orderBy]);

  //if page changed, request backend
  useEffect(() => {
    setLoading(true);
    axios
      .get(`/api/disasters?${request}&offset=${page * 9}&limit=9`)
      .then((res) => {
        let data = res.data;
        var x, item;
        var arr = [];
        for (x in data) {
          item = data[x];
          if (item.Disaster) {
            arr.push({
              disasterNumber: item.Disaster.fema_id,
              img: item.Disaster.image_url,
              type: item.Disaster.disaster_type,
              name: item.Disaster.name,
              date: item.Disaster.date,
              cost: item.Disaster.cost,
              season: item.Disaster.season,
              numPrograms: item.Disaster.programs.length,
            });
          }
        }
        setRows(arr);
        setLoading(false);
      })
      .catch(() => {
        setTest(true);
      });
  }, [page]);

  //if reset, reset search and filters
  useEffect(() => {
    if (reset === true) {
      setSearch("");
      setSeason("");
      setFilterType("");
      setOrderBy("placeholder");

      setRequest("");
      setNumDisasters(totalDisasters);
      setReset(false);
    }
  }, [reset]);

  if (loading) {
    return <CircularProgress />;
  } else if (test === true) {
    return (
      <div className={classes.root}>
        <h2>Server down.</h2>
      </div>
    );
  } else {
    return (
      <div className={classes.root}>
        <h1>Disasters</h1>
        <p>
          Click the button to search and click on the Name, cost, and disasterID
          to sort.
        </p>
        <SearchBar setSearch={setSearch} modelType="disasters" />
        <Filter
          filterName="season"
          selectList={SEASONS}
          handleChange={handleSeasonChange}
          filterVar={season}
        />
        <Filter
          filterName="type"
          selectList={DISASTER_TYPES}
          handleChange={handleTypeChange}
          filterVar={filterType}
        />
        <FormControl className={classes.formControl}>
          <Button
            onClick={(e) => {
              e.preventDefault();
              setReset(true);
            }}
            className={classes.button}
            type="submit"
          >
            Reset all
          </Button>
        </FormControl>
        <TableContainer>
          <Table
            className={classes.table}
            aria-labelledby="tableTitle"
            aria-label="enhanced table"
          >
            <EnhancedTableHead
              classes={classes}
              order={order}
              orderBy={orderBy}
              onRequestSort={handleRequestSort}
              rowCount={rows.length}
              headCells={DIS_GRID_HEAD}
            />
          </Table>
        </TableContainer>
        <Grid container spacing={8} className={classes.gridContainer}>
          {rows.map((row, index) => {
            return (
              <Grid item xs={12} sm={6} md={4} key={row.disasterNumber}>
                <DisasterCard disasterInfo={row} search={search} />
              </Grid>
            );
          })}
        </Grid>
        <TableContainer>
          <Table>
            <TableFooter>
              <TableRow>
                <TablePagination
                  rowsPerPageOptions={9}
                  colSpan={3}
                  count={numDisasters}
                  rowsPerPage={rowsPerPage}
                  page={page}
                  onChangePage={handleChangePage}
                  ActionsComponent={TablePaginationActions}
                />
              </TableRow>
            </TableFooter>
          </Table>
        </TableContainer>
      </div>
    );
  }
};
export default Disasters;
