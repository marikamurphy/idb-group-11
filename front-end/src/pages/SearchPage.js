import React, { useState, useEffect } from "react";
import { CircularProgress } from "@material-ui/core";
import SearchListTb from "../components/SearchListTb";
import DisasterListTb from "../components/DisasterListTb";
import axios from "axios";
import disPlaceHolder from "../assets/placeholders/disPlaceHolder.png";
import countyPlaceHolder from "../assets/placeholders/countyPlaceHolder.png";
import proPlaceHolder from "../assets/placeholders/proPlaceHolder.png";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    background: "#D2691E",
    color: "#DCDCDC",
    margin: "auto",
    padding: "150px",
    textAlign: "center",
  },
}));
//gives the result of searching the entire website
const SearchPage = (props) => {
  const classes = useStyles();
  const [loading, setLoading] = useState(false);
  const [disasters, setDisasters] = useState([]);
  const [programs, setPrograms] = useState([]);
  const [locations, setLocations] = useState([]);
  const id = props.match.params.id;
  const [test, setTest] = useState(false);

  //load result from backend
  useEffect(() => {
    setLoading(true);
    axios
      .get(`/api/search/${id}`)
      .then((res) => {
        let data = res.data;
        var x, item;
        var diss = [];
        var image;
        for (x in data.Disasters) {
          item = data.Disasters[x];
          if (item.Disaster) {
            image =
              !item.Disaster.image_url || item.Disaster.image_url === ""
                ? disPlaceHolder
                : item.Disaster.image_url;
            diss.push({
              name: item.Disaster.name,
              fema_id: item.Disaster.fema_id,
              img_url: image,
            });
          }
        }
        var counties = [];
        for (x in data.Counties) {
          item = data.Counties[x];
          if (item.Countie) {
            image =
              !item.Countie.image_url || item.Countie.image_url === ""
                ? countyPlaceHolder
                : item.Countie.image_url;
            counties.push({
              name: item.Countie.name,
              img_url: image,
              state: item.Countie.state,
              population: item.Countie.population,
              longitude: item.Countie.longitude,
              latitude: item.Countie.latitude,
            });
          }
        }
        var pros = [];
        for (x in data.Programs) {
          item = data.Programs[x];
          if (item.Program) {
            image =
              !item.Program.image_url || item.Program.image_url === ""
                ? proPlaceHolder
                : item.Program.image_url;
            pros.push({
              name: item.Program.name,
              img_url: image,
              type: item.Program.type,
              budget: item.Program.budget,
            });
          }
        }
        setDisasters(diss);
        setLocations(counties);
        setPrograms(pros);
        setLoading(false);
      })
      .catch(() => {
        setTest(true);
      });
  }, []);

  //put the results in 3 tables
  //ex. put disaster instances returned in the DisasterTb
  if (loading) {
    return <CircularProgress />;
  }
  if (test === true) {
    return (
      <div className={classes.root}>
        <h2>Server down.</h2>
      </div>
    );
  }
  return (
    <div className={classes.root}>
      <h1> Search Results</h1>
      <h3>Disasters</h3>
      <DisasterListTb items={disasters} search={id} />
      <br />
      <h3>Relief Programs</h3>
      <SearchListTb items={programs} instanceType={"programs"} search={id} />
      <br />
      <h3>Counties</h3>
      <SearchListTb items={locations} instanceType={"counties"} search={id} />
    </div>
  );
};
export default SearchPage;
