import React, { useState } from "react";
import wildfire from "../assets/wildfire.jpg";
import disasterLogo from "../assets/disaster.png";
import countyLogo from "../assets/county.png";
import programLogo from "../assets/program.jpeg";
import ready from "../assets/ready.png";
import ToolCard from "../components/ToolCard";
import { Grid, CircularProgress, Button } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { Redirect } from "react-router-dom";
import { QueryCache } from "react-query";

const models = [
  {
    //disasters
    name: "Disasters",
    intro: "Disaster instances",
    link: "/disasters",
    img: disasterLogo,
  },
  {
    //counties
    name: "Counties",
    intro: "Counties in the US",
    link: "/counties",
    img: countyLogo,
  },
  {
    //programs
    name: "Relief Programs",
    intro: "Relief programs for disasters",
    link: "/programs",
    img: programLogo,
  },
];

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    "& > * + *": {
      marginLeft: theme.spacing(2),
    },
    backgroundImage: `url(${wildfire})`,
    color: "blue",
    height: 1000,
    // width: "100%",
    backgroundPosition: "center",
    backgroundSize: "cover",
  },
  label: {
    textAlign: "center",
    margin: "auto",
    marginTop: "200px",
    marginLeft: "150px",
    color: "#DCDCDC",
    fontSize: "50pt",
  },
  grid: {
    margin: "auto",
    marginBottom: "100px",
  },
  safety: {
    margin: "auto",
    marginBottom: "100px",
    padding: "100px",
  },
  button: {
    background: "#DCDCDC",
    border: 0,
    color: "white",
  },
}));

const queryCache = new QueryCache();

//homepage
const Splash = () => {
  const [loading, setLoading] = useState(false);
  const [searchValue, setSearchValue] = useState("");
  const [redirect, setRedirect] = useState(false);

  const classes = useStyles();

  if (loading) {
    return (
      <div className={classes.root}>
        <CircularProgress />
      </div>
    );
  }

  if (redirect && searchValue !== "") {
    return <Redirect to={{ pathname: "/searchAll/" + searchValue }} />;
  }

  var searchOK = searchValue.length > 0;

  return (
    <div>
      <div className={classes.root}>
        <div className={classes.label}>
          DisasterInfo.me
          <form
            className="form-inline my-4 my-lg-0"
            onSubmit={(e) => {
              e.preventDefault();
            }}
          >
            <input
              width="120"
              className="form-control mr-sm-2"
              type="search"
              placeholder="Sitewise search"
              aria-label="Search"
              value={searchValue}
              onChange={(e) => {
                setSearchValue(e.target.value);
              }}
            />
            <Button
              className={classes.button}
              type="submit"
              disabled={!searchOK}
            >
              <a href={"/searchAll/" + searchValue}>Search</a>
            </Button>
          </form>
        </div>
        <div className={classes.grid}>
          <Grid container spacing={8} className="gridContainer">
            {models.map((row) => (
              <Grid item xs={12} sm={6} md={3}>
                <ToolCard toolInfo={row} />
              </Grid>
            ))}
          </Grid>
        </div>
      </div>
      <main className="container py-10">
        <div className={classes.safety}>
          <h2>Safety Procedures for common natural disasters:</h2>
          <br />
          <img src={ready} alt="Logo" width="50" />
          <a href="https://www.ready.gov/earthquakes">Earthquakes</a>
          <br />
          <img src={ready} alt="Logo" width="50" />
          <a href="https://www.ready.gov/wildfires">Wildfires</a>
          <br />
          <img src={ready} alt="Logo" width="50" />
          <a href="https://www.ready.gov/floods">Floods</a>
          <br />
          <img src={ready} alt="Logo" width="50" />
          <a href="https://www.ready.gov/hurricanes">Hurricanes</a>
          <br />
          <img src={ready} alt="Logo" width="50" />
          <a href="https://www.ready.gov/tornadoes">Tornadoes</a>
        </div>
      </main>
    </div>
  );
};

export default Splash;
