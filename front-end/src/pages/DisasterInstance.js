import React, { useState, useEffect } from "react";
import ListTb from "../components/ListTb";
import disPlaceHolder from "../assets/placeholders/disPlaceHolder.png";
import { CircularProgress, Paper, Grid } from "@material-ui/core";
import { Instance_style } from "../components/Styles";
import axios from "axios";

const DisasterInstance = (props) => {
  const classes = Instance_style();
  const [loading, setLoading] = useState(false);
  const [instanceInfo, setInstanceInfo] = useState({});
  const [programs, setPrograms] = useState([]);
  const [locations, setLocations] = useState([]);
  const [test, setTest] = useState(false);
  const id = props.match.params.id;
  //loading in this disaster instance
  useEffect(() => {
    setLoading(true);
    axios
      .get(`/api/disasters/${id}`)
      .then((res) => {
        let cs = res.data[0].counties.map((item, i) => {
          const instance = {
            name: item,
            img_url: res.data[0].county_image_urls[i],
          };
          return instance;
        });
        let pros = res.data[0].programs.map((item, i) => {
          const instance = {
            name: item,
            img_url: res.data[0].program_image_urls[i],
          };
          return instance;
        });
        setInstanceInfo(res.data[0]);
        setLocations(cs);
        setPrograms(pros);
        setLoading(false);
      })
      .catch(() => {
        setTest(true);
      });
  }, []);

  if (loading) {
    return (
      <div className={classes.root}>
        <CircularProgress />
      </div>
    );
  } else if (test === true) {
    return (
      <div className={classes.root}>
        <h2>Server down.</h2>
      </div>
    );
  }

  //if there's no image for this disaster, fill in the place holder image
  var image =
    !instanceInfo.image_url || instanceInfo.image_url === ""
      ? disPlaceHolder
      : instanceInfo.image_url;
  //each disaster instance has some attributes, a list of associated programs
  // and a list of affected counties.
  return (
    <div className={classes.root}>
      <h1> {instanceInfo.name}</h1>
      <Paper className={classes.paper}>
        <Grid container spacing={4} elevation={0}>
          <Grid item xs={6}>
            <img
              className={classes.img}
              src={image}
              alt={instanceInfo.name}
              width="500"
            />
          </Grid>
          <Grid item xs={6}>
            <iframe
              className="embed-responsive-item"
              src={instanceInfo.video_url}
              allowFullScreen
              title="disasterVideo"
              width="500"
              height="400"
            ></iframe>
          </Grid>
        </Grid>
      </Paper>
      <p className={classes.attributes}>
        <br />
        Type: {instanceInfo.disaster_type}
        <br />
        Disaster ID: {props.match.params.id}
        <br />
        Time:
        {instanceInfo.date}
        <br />
        Season:
        {instanceInfo.season}
        <br />
        Cost: {instanceInfo.cost}
        <br />
      </p>
      <br />
      <h3>Relief Programs</h3>
      <ListTb items={programs} instanceType={"programs"} />
      <br />
      <h3>Counties Impacted</h3>
      <ListTb items={locations} instanceType={"counties"} />
    </div>
  );
};

export default DisasterInstance;
