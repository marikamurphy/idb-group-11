import React, { useState, useEffect } from "react";
import { Grid, CircularProgress } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import ZongyingMo from "../assets/members/ZongyingMo.jpeg";
import MarikaMurphy from "../assets/members/MarikaMurphy.jpg";
import BrandonN from "../assets/members/BrandonN.jpg";
import awsLogo from "../assets/aws.png";
import gitlabLogo from "../assets/gitlab.jpeg";
import postmanLogo from "../assets/postman.jpg";
import flaskLogo from "../assets/flask.jpg";
import dockerLogo from "../assets/docker.png";
import reactLogo from "../assets/react.jpg";
import arcgis from "../assets/arcgis.png";
import reliefweb from "../assets/reliefweb.jpeg";
import fema from "../assets/fema.png";
import MemberCard from "../components/MemberCard";
import ToolCard from "../components/ToolCard";

const useStyles = makeStyles({
  root: {
    background: "#D2691E",
    color: "#DCDCDC",
    margin: "auto",
    padding: "150px",
    textAlign: "center",
  },
  section: {
    marginTop: "100px",
  },
});

const About = () => {
  const classes = useStyles();
  const [loading, setLoading] = useState(false);
  const [test, setTest] = useState(false);

  const [commits, setCommits] = useState([0, 0, 0]);

  //get stats from gitlab
  useEffect(() => {
    setLoading(true);
    fetch(`https://gitlab.com/api/v4/projects/21344288/repository/contributors`)
      .then((response) => response.json())
      .then((data) => {
        var zmCommit;
        var bCommit;
        var mmCommit;
        var item;
        for (item of data) {
          if (item.email === "marikamrphy18@gmail.com") {
            mmCommit = item.commits;
          }
          if (item.name === "Zongying Mo") {
            zmCommit = item.commits;
          }
          if (item.name === "Brandon0329") {
            bCommit = item.commits;
          }
        }
        setCommits([mmCommit, bCommit, zmCommit]);
        setLoading(false);
      })
      .catch(() => {
        setTest(true);
      });
  }, []);

  //member info
  let zm = {
    name: "Zongying Mo",
    intro:
      "Hi, I'm Zongying Mo. I am a senior computer science major. \
        I worked primarily on the frontend and some AWS-related tasks.",
    commits: commits[2],
    img: ZongyingMo,
    linkedin: "https://www.linkedin.com/in/zongying-mo-2380a5173/",
  };
  let mm = {
    name: "Marika Murphy",
    intro:
      "Hi, I'm Marika. I am a third year computer science major at the \
        University of Texas at Austin. I worked primarily on the backend \
        for this application. I am also a peer mentor for the Autonomous \
        Robotics FRI program.",
    commits: commits[0],
    img: MarikaMurphy,
    linkedin: "https://www.linkedin.com/in/marika-murphy/",
  };
  let bn = {
    name: "Brandon Nsidinanya",
    intro:
      "Hi, I'm Brandon, I'm a senior computer science major, and I worked \
        on the databases, the API, and AWS-related tasks.",
    commits: commits[1],
    img: BrandonN,
    linkedin: "https://www.linkedin.com/in/brandon-nsidinanya-794b34174/",
  };
  //tools:
  let tools = [
    {
      //aws
      name: "Amazon Web Services",
      intro: "Hosting server for the website and database",
      link: "https://aws.amazon.com/",
      img: awsLogo,
    },
    {
      //git
      name: "Gitlab",
      intro: "Gitlab source code, Version control, issue tracking & CI",
      link: "https://gitlab.com/marikamurphy/idb-group-11",
      img: gitlabLogo,
    },
    {
      //postman
      name: "Postman",
      intro: "Our API design and documentation",
      link: "https://documenter.getpostman.com/view/12859896/TVYQ3Erb",
      img: postmanLogo,
    },
    {
      //react
      name: "React",
      intro: "Built frontend",
      link: "https://reactjs.org/",
      img: reactLogo,
    },
    {
      //docker
      name: "Docker",
      intro: "Container for backend",
      link: "https://hub.docker.com/",
      img: dockerLogo,
    },
    {
      //flask
      name: "Flask",
      intro: "Built server side of RESTful API",
      link: "https://flask.palletsprojects.com/en/1.1.x/",
      img: flaskLogo,
    },
  ];

  let sources = [
    {
      name: "OpenFema",
      intro: "Main api",
      link: "https://www.fema.gov/about/openfema/data-sets",
      img: fema,
    },
    {
      name: "ReliefWeb",
      intro: "api",
      link: "https://reliefweb.int/help/api",
      img: reliefweb,
    },
    {
      name: "ArcGis",
      intro: "api",
      link:
        "https://developers.arcgis.com/python/sample-notebooks/mapping-recent-natural-disasters/",
      img: arcgis,
    },
  ];

  if (loading) {
    return (
      <div className={classes.root}>
        <CircularProgress />
      </div>
    );
  }
  if (test === true) {
    return (
      <div className={classes.root}>
        <h2>Server down.</h2>
      </div>
    );
  }

  return (
    <div className={classes.root}>
      <h1>About DisasterInfo.me</h1>
      <p>
        In light of the recent wild fire in California and Australia, we got
        curious about natural disasters. So we built this website to answer the
        following questions we had: Where have natural disasters occurred in the
        world? Which natural disasters are common to particular cities/areas?
        What organizations serve which areas with which kinds of natural
        disasters? We hope the information we provide help spread awareness
        regarding natural disaster precautions and prepare people with the
        necessary knowledge of the natural disasters that may occur at home.
      </p>
      <div className={classes.section}>
        <h2>Members</h2>
        <p>All members are team leads.</p>

        <Grid container spacing={8} className="gridContainer">
          <Grid item xs={12} sm={6} md={4}>
            <MemberCard memberInfo={mm} />
          </Grid>
          <Grid item xs={12} sm={6} md={4}>
            <MemberCard memberInfo={bn} />
          </Grid>
          <Grid item xs={12} sm={6} md={4}>
            <MemberCard memberInfo={zm} />
          </Grid>
        </Grid>
      </div>
      <div className={classes.section}>
        <h2 style={{ marginBottom: "50px" }}>Tools</h2>
        <Grid container spacing={8} className="gridContainer">
          {tools.map((row) => (
            <Grid item xs={12} sm={6} md={3}>
              <ToolCard toolInfo={row} />
            </Grid>
          ))}
        </Grid>
      </div>
      <div className={classes.root}>
        <br />
        <h2>Data Sources</h2>
        <br />
        <p>
          For data collection, we used Python. Images and video URLs were
          scraped using Selenium, and Twitter URLs using google.
        </p>
        <Grid container spacing={8} className="gridContainer">
          {sources.map((row) => (
            <Grid item xs={12} sm={6} md={3}>
              <ToolCard toolInfo={row} />
            </Grid>
          ))}
        </Grid>
      </div>
    </div>
  );
};

export default About;
