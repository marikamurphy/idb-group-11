import React, { useState, useEffect } from "react";
import ListTb from "../components/ListTb";
import DisasterListTb from "../components/DisasterListTb";
import ShowTwitter from "../components/ShowTwitter";
import { CircularProgress, Paper, Grid } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import axios from "axios";
import { Instance_style } from "../components/Styles";

//each location instance has a list or related disasters, associated counties
//for media there will be a map and a tweet
const LocationInstance = (props) => {
  const classes = Instance_style();
  const [loading, setLoading] = useState(false);
  const [instanceInfo, setInstanceInfo] = useState({});
  const [programs, setPrograms] = useState([]);
  const [disasters, setDisasters] = useState([]);
  const [tweet, setTweet] = useState("");
  const id = props.match.params.id.toUpperCase();

  //load this instance
  useEffect(() => {
    setLoading(true);
    axios.get(`/api/counties?name=${id}`).then((res) => {
      let dis = res.data[0].County.disasters.map((item, i) => {
        const instance = {
          name: item.name,
          fema_id: res.data[0].County.disaster_ids[i],
          img_url: res.data[0].County.disaster_image_urls[i],
        };
        return instance;
      });

      if (res.data[0].County.programs[0] !== "") {
        let pros = res.data[0].County.programs.map((item, i) => {
          const instance = {
            name: item,
            img_url: res.data[0].County.program_image_urls[i],
          };
          return instance;
        });
        setPrograms(pros);
      }

      setInstanceInfo(res.data[0].County);
      setDisasters(dis);

      setTweet(res.data[0].County.twitter_url);
      setLoading(false);
    });
  }, []);

  if (loading) {
    return <CircularProgress />;
  }
  //for google map
  var site =
    "https://maps.google.com/maps?width=100%25&height=600&hl=en&q=" +
    encodeURIComponent(instanceInfo.name) +
    "+(my)&t=&amp;z=14&ie=UTF8&iwloc=B&output=embed";

  return (
    <div className={classes.root}>
      <h1> {instanceInfo.name}</h1>
      <Paper className={classes.paper}>
        <Grid container spacing={4} elevation={0}>
          <Grid item xs={6}>
            <ShowTwitter t={tweet} />
          </Grid>

          <Grid item xs={6}>
            <iframe
              width="500"
              height="400"
              frameBorder="0"
              scrolling="no"
              marginHeight="0"
              marginWidth="0"
              src={site}
              title="mapForLocation"
            ></iframe>
          </Grid>
        </Grid>
      </Paper>
      <p className={classes.attributes}>
        <br />
        Code: {instanceInfo.code}
        <br />
        Latitude:
        {instanceInfo.latitude}
        <br />
        Longitude: {instanceInfo.longitude}
        <br />
        Population:
        {instanceInfo.population}
        <br />
        State:
        {instanceInfo.state}
        <br />
        Tweets:
        <br />
      </p>

      <h3>Relief Programs</h3>
      <ListTb items={programs} instanceType={"programs"} />
      <br />
      <h3>Disasters</h3>
      <DisasterListTb items={disasters} search={""} />
    </div>
  );
};

export default LocationInstance;
