import React from "react";
import ReactDOM from "react-dom";
import Splash from "./pages/Splash";
import Disasters from "./pages/Disasters";
import Programs from "./pages/Programs";
import About from "./pages/About";
import Locations from "./pages/Locations";

it("Renders Splash", () => {
  const div = document.createElement("div");
  ReactDOM.render(<Splash />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it("Renders Disasters", () => {
  const div = document.createElement("div");
  ReactDOM.render(<Disasters />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it("Renders Programs", () => {
  const div = document.createElement("div");
  ReactDOM.render(<Programs />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it("Renders Locations", () => {
  const div = document.createElement("div");
  ReactDOM.render(<Locations />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it("Renders About", () => {
  const div = document.createElement("div");
  ReactDOM.render(<About />, div);
  ReactDOM.unmountComponentAtNode(div);
});
