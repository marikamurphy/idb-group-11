import React from "react";
import Highlighter from "react-highlight-words";

const Highlight = (props) => {
  return (
    <Highlighter
      highlightClassName="highlighter"
      searchWords={props.search.trim().split(" ")}
      autoEscape={true}
      textToHighlight={props.text}
    />
  );
};

export default Highlight;
