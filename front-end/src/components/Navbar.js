import React from "react";
import capD from "../assets/capD.png";
//code from Bootstrap Navbar

function Navbar() {
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <a className="navbar-brand" href="/">
        <img src={capD} alt="website_logo" width="30" />
        DisasterInfo
      </a>
      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span className="navbar-toggler-icon"></span>
      </button>
      <div className="collapse navbar-collapse" id="navbarSupportedContent">
        <ul className="navbar-nav mr-auto">
          <li className="nav-item">
            <a className="nav-link" href="/disasters">
              Disasters
            </a>
          </li>
          <li className="nav-item">
            <a className="nav-link" href="/counties">
              Locations
            </a>
          </li>
          <li className="nav-item">
            <a className="nav-link" href="/programs">
              Relief Programs
            </a>
          </li>
          <li className="nav-item">
            <a className="nav-link" href="/about">
              About
            </a>
          </li>
          <li className="nav-item">
            <a className="nav-link" href="/visualization">
              Visualization
            </a>
          </li>
        </ul>
      </div>
    </nav>
  );
}

export default Navbar;
