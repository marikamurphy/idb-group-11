import DescendingComparator from "./DescendingComparator";

function GetComparator(order, orderBy) {
  return order === "desc"
    ? (a, b) => DescendingComparator(a, b, orderBy)
    : (a, b) => -DescendingComparator(a, b, orderBy);
}

export default GetComparator;
