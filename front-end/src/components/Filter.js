import React from "react";
import { MenuItem, Select, InputLabel, FormControl } from "@material-ui/core";
import { Form_style } from "./Styles";
function Filter(props) {
  const { filterName, selectList, handleChange, filterVar } = props;
  const classes = Form_style();

  return (
    <FormControl className={classes.formControl}>
      <InputLabel>{filterName}</InputLabel>
      <Select
        labelId="demo-simple-select-label"
        id="demo-simple-select"
        value={filterVar}
        onChange={handleChange}
        inputProps={{
          name: filterName,
          id: "-native-simple",
        }}
      >
        {selectList.map((item) => (
          <MenuItem value={item} onClick={handleChange} key={item}>
            {item}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
}
export default Filter;
