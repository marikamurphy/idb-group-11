import React from "react";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableFooter from "@material-ui/core/TableFooter";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import { Link } from "react-router-dom";
import TablePaginationActions from "./TablePaginationActions";
import Highlight from "./Highlight";

// Show the list of related disasters in a program/county instance

// The only difference between the disasterListTb and the ListTb is
// the link component in render. This is because in the json, the
// "disasters" are list of objects in the json but "counties" and "programs"
// are list of strings. So we had to index into the "disasters" in
// disasterListTb.

//code of entire file is basically from material-ui tables.

//styling

const useStyles2 = makeStyles({
  table: {
    minWidth: 400,
  },
  image: {
    width: 140,
    height: 70,
  },
});

//the actual table that has the list of related disasters
const DisasterListTb = ({ items, search }) => {
  const classes = useStyles2();
  const rows = items;

  //for pagination, code snippet from material-ui tables
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const emptyRows =
    rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="custom pagination table">
        <TableBody>
          {(rowsPerPage > 0
            ? rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
            : rows
          ).map((row) => (
            <TableRow key={row.id}>
              <TableCell size="small">
                <img
                  className={classes.image}
                  src={row.img_url}
                  alt={row.name}
                />
              </TableCell>
              <TableCell align="left">
                <Link to={`/disasters/${row.fema_id}`}>
                  <Highlight search={search} text={row.name} />
                </Link>
              </TableCell>
            </TableRow>
          ))}

          {emptyRows > 0 && (
            <TableRow style={{ height: 53 * emptyRows }}>
              <TableCell colSpan={6} />
            </TableRow>
          )}
        </TableBody>
        <TableFooter>
          <TableRow>
            <TablePagination
              rowsPerPageOptions={[5, 10, 25, { label: "All", value: -1 }]}
              colSpan={3}
              count={rows.length}
              rowsPerPage={rowsPerPage}
              page={page}
              SelectProps={{
                inputProps: { "aria-label": "rows per page" },
                native: true,
              }}
              onChangePage={handleChangePage}
              onChangeRowsPerPage={handleChangeRowsPerPage}
              ActionsComponent={TablePaginationActions}
            />
          </TableRow>
        </TableFooter>
      </Table>
    </TableContainer>
  );
};

export default DisasterListTb;
