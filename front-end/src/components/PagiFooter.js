import React, { useState, useEffect } from "react";
import {
  Table,
  TableContainer,
  TableFooter,
  TablePagination,
  TableRow,
} from "@material-ui/core";
import TablePaginationActions from "../components/TablePaginationActions";

function PagiFooter(props) {
  const { rowsPerPage, totalNum, page, handleChangePage } = props;
  <TableContainer>
    <Table>
      <TableFooter>
        <TableRow>
          <TablePagination
            rowsPerPageOptions={9}
            colSpan={3}
            count={totalNum}
            rowsPerPage={rowsPerPage}
            page={page}
            onChangePage={handleChangePage}
            ActionsComponent={TablePaginationActions}
          />
        </TableRow>
      </TableFooter>
    </Table>
  </TableContainer>;
}
export default PagiFooter;
