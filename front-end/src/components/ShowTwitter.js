import React from "react";
import { TwitterTimelineEmbed, TwitterTweetEmbed } from "react-twitter-embed";

//for different types of twitter links
//do twitter timeline or tweets
//if no twitter link, have the explanantion
function ShowTwitter(props) {
  var t = props.t;
  if (t === "") {
    return (
      <p>
        "There's a limit for how many tweets we can scrape so no tweets for this
        county :("
      </p>
    );
  } else if (t.split("/").length < 6) {
    t = t.split("/")[3].split("?")[0];
    return (
      <TwitterTimelineEmbed
        sourceType="profile"
        screenName={t}
        options={{ height: 400, width: 500 }}
      />
    );
  }
  t = t.split("/")[5].split("?")[0];
  return (
    <TwitterTweetEmbed tweetId={t} options={{ height: 400, width: 500 }} />
  );
}

export default ShowTwitter;
