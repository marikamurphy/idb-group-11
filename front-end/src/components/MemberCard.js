import React from "react";
import { Card, CardContent, CardMedia, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

//styling
const useStylesM = makeStyles({
  root: {
    maxWidth: 300,
    height: 670,
  },
  bullet: {
    display: "inline-block",
    margin: "0 2px",
    transform: "scale(0.8)",
  },
  title: {
    fontSize: 14,
  },
  media: {
    height: 400,
  },
  pos: {
    marginBottom: 12,
  },
  gridContainer: {
    paddingLeft: "40px",
    paddingRight: "40px",
  },
});

//card for members
function MemberCard(props) {
  const classes = useStylesM();
  const memberInfo = props.memberInfo;

  return (
    <Card className={classes.root}>
      <CardMedia
        className={classes.media}
        image={memberInfo.img}
        title={memberInfo.name}
      />
      <CardContent>
        <Typography gutterBottom variant="h5" component="h2">
          {memberInfo.name}
        </Typography>
        <Typography variant="body2" component="p">
          intro: {memberInfo.intro}
          <br />
          <br />
          commits: {memberInfo.commits}
          <br />
          <a href={memberInfo.linkedin}> LinkedIn </a>
        </Typography>
      </CardContent>
    </Card>
  );
}
export default MemberCard;
