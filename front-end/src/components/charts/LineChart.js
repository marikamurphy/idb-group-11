import React, { useState, useEffect } from "react";
import FusionCharts from "fusioncharts";
import ReactFC from "react-fusioncharts";
import charts from "fusioncharts/fusioncharts.charts";

charts(FusionCharts);

function LineChart(props) {
  const dataSource = {
    chart: {
      //Set the chart caption
      caption: "Disaster instances per year in the US",
      //Set the chart subcaption
      subCaption: "Total number",
      //Set the x-axis name
      xAxisName: "Year",
      //Set the y-axis name
      yAxisName: "Number of disasters",
      // numberSuffix: "K",
      //Set the theme for your chart
      // theme: "fusion"
    },
    // Chart Data
    data: props.chartData,
  };
  const chartConfigs = {
    type: "line", // The chart type
    width: "800", // Width of the chart
    height: "600", // Height of the chart
    dataFormat: "json", // Data type
    dataSource: dataSource,
  };
  return <ReactFC {...chartConfigs} />;
}

export default LineChart;
