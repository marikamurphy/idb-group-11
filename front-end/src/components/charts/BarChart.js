import React from "react";
import FusionCharts from "fusioncharts";
import Charts from "fusioncharts/fusioncharts.charts";
import ReactFC from "react-fusioncharts";
import FusionTheme from "fusioncharts/themes/fusioncharts.theme.fusion";

ReactFC.fcRoot(FusionCharts, Charts, FusionTheme);

function BarChart(props) {
  const chartConfigs = {
    type: "column2d",
    width: 800,
    height: 600,
    dataFormat: "json",
    dataSource: props.chartData,
  };
  return <ReactFC {...chartConfigs} />;
}

export default BarChart;
