import React from "react";
import FusionCharts from "fusioncharts";
import Charts from "fusioncharts/fusioncharts.charts";
import ReactFC from "react-fusioncharts";
import FusionTheme from "fusioncharts/themes/fusioncharts.theme.fusion";

// Resolves charts dependancy
Charts(FusionCharts);
// ReactFC.fcRoot(FusionCharts, Charts, FusionTheme);

function PieChart(props) {
  const chartConfigs = {
    type: "pie2d",
    width: 800,
    height: 600,
    dataFormat: "json",
    dataSource: props.chartData,
  };
  return <ReactFC {...chartConfigs} />;
}

export default PieChart;

// class MyComponent extends React.Component {
//   render() {
//     return (
//       <ReactFusioncharts
//         type="pie2d"
//         width="100%"
//         height="100%"
//         dataFormat="JSON"
//         dataSource={dataSource}
//       />
//     );
//   }
// }
