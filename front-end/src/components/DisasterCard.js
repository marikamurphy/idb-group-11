import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  Button,
  Card,
  CardActions,
  CardActionArea,
  CardContent,
  CardMedia,
  Typography,
} from "@material-ui/core";
import { Link } from "react-router-dom";

//for showing disaster
const useStyles = makeStyles((theme) => ({
  root: {
    width: 400,
    height: 400,
  },
  paper: {
    width: "100%",
    marginBottom: theme.spacing(2),
  },
  bullet: {
    display: "inline-block",
    margin: "0 2px",
    transform: "scale(0.8)",
  },
  title: {
    fontSize: 14,
  },
  media: {
    height: 140,
  },
  pos: {
    marginBottom: 12,
  },
  gridContainer: {
    paddingLeft: "40px",
    paddingRight: "40px",
  },
}));

//disaster Card: shows an image and some attributes
function DisasterCard(props) {
  const classes = useStyles();
  const disasterInfo = props.disasterInfo;
  //for highlighting searched terms
  var Highlight = require("react-highlighter");
  return (
    <Link to={`/disasters/${disasterInfo.disasterNumber}`}>
      <Card className={classes.root}>
        <CardActionArea>
          <CardMedia
            className={classes.media}
            image={disasterInfo.img}
            title={disasterInfo.name}
          />
          <CardContent>
            <Typography gutterBottom variant="h5" component="h2">
              <Highlight search={props.search}>{disasterInfo.name}</Highlight>
            </Typography>
            <Typography variant="body2" color="textSecondary" component="p">
              <Highlight search={props.search}>
                Type: {disasterInfo.type}
                <br />
                Disaster ID: {disasterInfo.disasterNumber}
                <br />
                Cost: {disasterInfo.cost}
                <br />
                Time: {disasterInfo.date}
                <br />
                Season: {disasterInfo.season}
                <br />
                Number of programs: {disasterInfo.numPrograms}
              </Highlight>
            </Typography>
          </CardContent>
        </CardActionArea>

        <CardActions>
          <Button size="small" color="primary">
            Learn More
          </Button>
        </CardActions>
      </Card>
    </Link>
  );
}

export default DisasterCard;
