import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import { Link } from "react-router-dom";

const useStyles2 = makeStyles({
  table: {
    minWidth: 500,
  },
  image: {
    width: 140,
    height: 70,
  },
});

//return different rows for different instanceTypes
const CustomRow = ({ row, instanceType, search }) => {
  const classes = useStyles2();
  var Highlight = require("react-highlighter");
  if (instanceType === "programs") {
    return (
      <TableRow key={row.id}>
        <TableCell size="small">
          <img className={classes.image} src={row.img_url} alt={row.name} />
        </TableCell>
        <TableCell component="th" scope="row">
          <Link to={`/${instanceType}/${encodeURIComponent(row.name)}`}>
            <Highlight search={search}>{row.name}</Highlight>
          </Link>
        </TableCell>
        <TableCell>
          <Highlight search={search}> {row.budget}</Highlight>
        </TableCell>
        <TableCell>
          {" "}
          <Highlight search={search}>{row.type}</Highlight>
        </TableCell>
        <TableCell>
          <Highlight search={search}> {row.counties}</Highlight>
        </TableCell>
        <TableCell>
          <Highlight search={search}> {row.disaster}</Highlight>
        </TableCell>
      </TableRow>
    );
  } else {
    return (
      <TableRow key={row.id}>
        <TableCell size="small">
          <img className={classes.image} src={row.img_url} alt={row.name} />
        </TableCell>
        <TableCell component="th" scope="row">
          <Link to={`/${instanceType}/${encodeURIComponent(row.name)}`}>
            <Highlight search={search}>{row.name}</Highlight>
          </Link>
        </TableCell>
        <TableCell>
          {/* {" "} */}
          <Highlight search={search}>{row.population}</Highlight>
        </TableCell>
        <TableCell>
          <Highlight search={search}>{row.state}</Highlight>
        </TableCell>
        <TableCell>
          <Highlight search={search}>{row.longitude}</Highlight>
        </TableCell>
        <TableCell>
          <Highlight search={search}>{row.latitude}</Highlight>
        </TableCell>
      </TableRow>
    );
  }
};
export default CustomRow;
