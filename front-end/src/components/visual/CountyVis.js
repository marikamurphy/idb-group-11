import React, { useState, useEffect } from "react";
import { MapContainer, CircleMarker, TileLayer, Tooltip } from "react-leaflet";
import "leaflet/dist/leaflet.css";
import { CircularProgress } from "@material-ui/core";
import axios from "axios";

const data = {
  minLat: 25.5,
  maxLat: 48.99,
  minLong: -125.31,
  maxLong: -65.76,
};

const CountyVis = () => {
  var centerLat = (data.minLat + data.maxLat) / 2;
  var centerLong = (data.minLong + data.maxLong) / 2;
  const [countyChartData, setCountyChartData] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    axios.get(`/api/counties/visual`).then((res) => {
      let data = eval(res.data);
      var arr = [];
      var c = 0;
      data.map((cur) => {
        if (!isNaN(cur.coordinates[0]) && !isNaN(cur.coordinates[1])) {
          arr.push({
            name: cur.name,
            coordinates: [cur.coordinates[0], cur.coordinates[1]],
            population: cur.population,
          });
        }
      });
      setCountyChartData(arr);
      setLoading(false);
    });
  }, []);
  if (loading) {
    return (
      <div>
        <CircularProgress />
      </div>
    );
  }

  return (
    <div>
      <MapContainer
        style={{ height: "600px", width: "800px" }}
        zoom={4}
        center={[centerLat, centerLong]}
      >
        <TileLayer url="http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />
        {countyChartData.map((city, k) => (
          <CircleMarker
            key={k}
            center={[city["coordinates"][1], city["coordinates"][0]]}
            radius={Math.log(city["population"] / 10000)}
            fillOpacity={0.5}
            stroke={false}
          >
            <Tooltip direction="right" offset={[-8, -2]} opacity={1}>
              <span>
                {city["name"] + ": " + "Population" + " " + city["population"]}
              </span>
            </Tooltip>
          </CircleMarker>
        ))}
      </MapContainer>
    </div>
  );
};

export default CountyVis;
