import React, { useState, useEffect } from "react";
import { CircularProgress } from "@material-ui/core";
import BarChart from "../charts/BarChart";
import axios from "axios";
import PieChart from "../charts/PieChart";
import BubbleChart from "@weknow/react-bubble-chart-d3";

const fishStatuses = new Map([
  ["DD", "DATA DEFICIENT"],
  ["LC", "LEAST CONCERN"],
  ["NT", "NEAR THREATENED"],
  ["VU", "VULNERABLE"],
  ["EN", "ENDANGERED"],
  ["CR", "CRITICALLY ENDANGERED"],
  ["EW", "EXTINCT IN THE WILD"],
  ["EX", "EXTINCT"],
  ["NE", "NOT EVALUATED"],
]);

const WatersChart = () => {
  const [waterData, setWaterData] = useState({});
  // const [waterChartData, setWaterChartData] = useState(null);
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    setLoading(true);
    var waters = [];
    var x;
    axios.get(`https://www.conservocean.me/api/water`).then((res) => {
      let data = res.data.data;
      var statuses = new Map();
      var status;
      data.map((water) => {
        status = water.type;
        if (statuses.has(status)) {
          statuses.set(status, statuses.get(status) + 1);
        } else {
          statuses.set(status, 1);
        }
      });
      statuses.forEach(function (value, key) {
        waters.push({ label: key.toString(), value: value.toString() });
      });

      setWaterData(waters);
      setLoading(false);
    });
  }, []);

  if (loading) {
    return (
      <div>
        <CircularProgress />
      </div>
    );
  }
  const waterChartData = {
    chart: {
      caption: "Number of Water Bodies per Type",
      subCaption: "Worldwide",
      xAxisName: "Number of water bodies",
      yAxisName: "Type",
      theme: "fusion",
    },
    data: waterData,
  };
  return <BarChart chartData={waterChartData} />;
};

const ImpactsChart = () => {
  const [impactData, setImpactData] = useState(null);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    var impacts = [];
    axios.get(`https://www.conservocean.me/api/human`).then((res) => {
      let data = res.data.data;
      var statuses = new Map();
      var status;
      data.map((impact) => {
        status = impact.subcategory;
        if (statuses.has(status)) {
          statuses.set(status, statuses.get(status) + 1);
        } else {
          statuses.set(status, 1);
        }
      });
      statuses.forEach(function (value, key) {
        impacts.push({ label: key.toString(), value: value.toString() });
      });
      setImpactData(impacts);
      setLoading(false);
    });
  }, []);

  // //impact - Pie
  const impactChartData = {
    chart: {
      caption: "Number of Human Impacts per Subcategory",
      plottooltext: "<b>$percentValue</b> of human impacts are $label",
      showlegend: "1",
      showpercentvalues: "1",
      legendposition: "bottom",
      usedataplotcolorforlabels: "1",
    },
    data: impactData,
  };
  if (loading || !impactData) {
    return (
      <div>
        <CircularProgress />
      </div>
    );
  }
  return <PieChart chartData={impactChartData} />;
};

const FishesChart = () => {
  const [loading, setLoading] = useState(false);
  const [fishData, setFishData] = useState([]);

  //load in data
  useEffect(() => {
    setLoading(true);
    //- fish
    var fishes = [];

    axios.get(`https://www.conservocean.me/api/fish`).then((res) => {
      let data = res.data.data;
      var statuses = new Map();
      var status;
      data.map((fish) => {
        status = fishStatuses.get(fish.endanger_status);
        if (statuses.has(status)) {
          statuses.set(status, statuses.get(status) + 1);
        } else {
          statuses.set(status, 1);
        }
      });
      statuses.forEach(function (value, key) {
        fishes.push({ label: key.toString(), value: value.toString() });
      });
      setFishData(fishes);
      setLoading(false);
    });
  }, []);

  if (loading) {
    return (
      <div>
        <CircularProgress />
      </div>
    );
  }
  return (
    <BubbleChart
      graph={{
        zoom: 1.1,
        offsetX: -0.05,
        offsetY: -0.01,
      }}
      width={800}
      height={600}
      padding={0} // optional value, number that set the padding between bubbles
      showLegend={true} // optional value, pass false to disable the legend.
      legendPercentage={20} // number that represent the % of with that legend going to use.
      legendFont={{
        family: "Arial",
        size: 12,
        color: "#000",
        weight: "bold",
      }}
      valueFont={{
        family: "Arial",
        size: 12,
        color: "#fff",
        weight: "bold",
      }}
      labelFont={{
        family: "Arial",
        size: 16,
        color: "#fff",
        weight: "bold",
      }}
      // //Custom bubble/legend click functions such as searching using the label, redirecting to other page
      // bubbleClickFunc={this.bubbleClick}
      // legendClickFun={this.legendClick}
      data={fishData}
    />
  );
};
export { WatersChart, ImpactsChart, FishesChart };
