import React, { useState, useEffect } from "react";
import { CircularProgress } from "@material-ui/core";
import BarChart from "../charts/BarChart";
import LineChart from "../charts/LineChart";
import axios from "axios";

const disDataSource = {
  chart: {},
  caption: {
    text: "Number of Disaster Instances per Year",
  },
  subcaption: {
    text: "in the US",
  },
  yaxis: [
    {
      plot: {
        value: "Number of disasters",
      },
      title: "Number of disasters",
    },
  ],
};

const DisastersChart = () => {
  const [disChartData, setDisChartData] = useState({});
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    setLoading(true);
    var x;
    axios.get(`/api/disasters/visual`).then((res) => {
      let data = res.data;
      var arr = [];
      for (x in data) {
        arr.push({ label: x, value: data[x].toString() });
      }
      setDisChartData(arr);
      setLoading(false);
    });
  }, []);

  return <LineChart chartData={disChartData} />;
};

const ProgramsChart = () => {
  const [proData, setProData] = useState({});
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    setLoading(true);
    axios.get(`/api/programs/visual`).then((res) => {
      var x;
      var arr = [];
      let data = res.data.programs_per_state;
      for (x in data) {
        arr.push({ label: x, value: data[x].toString() });
      }
      setProData(arr);
      setLoading(false);
    });
  }, []);
  //programs - Bar
  const proChartData = {
    chart: {
      caption: "Number of Programs per State",
      subCaption: "in the US",
      xAxisName: "States",
      yAxisName: "Number of programs",
      theme: "fusion",
    },
    data: proData,
  };

  if (loading) {
    return (
      <div>
        <CircularProgress />
      </div>
    );
  }
  return <BarChart chartData={proChartData} />;
};

export { ProgramsChart, DisastersChart };
