import React from "react";
import {
  Card,
  CardActionArea,
  CardContent,
  CardMedia,
  Typography,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

//styling
const useStylesT = makeStyles({
  root: {
    maxWidth: 300,
    height: 330,
  },
  bullet: {
    display: "inline-block",
    margin: "0 2px",
    transform: "scale(0.8)",
  },
  title: {
    fontSize: 14,
  },
  media: {
    maxHeight: 220,
    width: "100%",
  },
  pos: {
    marginBottom: 12,
  },
  gridContainer: {
    paddingLeft: "40px",
    paddingRight: "40px",
  },
});

//card for tools
function ToolCard(props) {
  const classes = useStylesT();
  const toolInfo = props.toolInfo;

  return (
    <Card className={classes.root}>
      <CardActionArea>
        <a href={toolInfo.link}>
          <CardMedia
            component="img"
            className={classes.media}
            image={toolInfo.img}
            title={toolInfo.name}
          />
        </a>
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            {toolInfo.name}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            {toolInfo.intro}
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  );
}

export default ToolCard;
