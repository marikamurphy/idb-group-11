import React from "react";
import { makeStyles } from "@material-ui/core/styles";

const Instance_style = makeStyles({
  root: {
    background: "#D2691E",
    color: "#DCDCDC",
    margin: "auto",
    padding: "100px",
    paddingLeft: "150px",
    paddingRight: "150px",
    textAlign: "center",
  },
  paper: {
    margin: "auto",
    paddingTop: "30px",
    background: "#D2691E",
    color: "#DCDCDC",
    elevation: 0,
  },
  picture: {
    alignContent: "left",
  },
  attributes: {
    padding: "50px",
    fontSize: 20,
  },
});

const Grid_style = makeStyles((theme) => ({
  root: {
    background: "#D2691E",
    color: "#DCDCDC",
    margin: "auto",
    padding: "150px",
    textAlign: "center",
  },
  table: {
    minWidth: 600,
  },
  gridContainer: {
    paddingLeft: "40px",
    paddingRight: "40px",
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  button: {
    background: "#DCDCDC",
    border: 0,
    color: "black",
  },
}));

const Form_style = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  button: {
    background: "#DCDCDC",
    border: 0,
    color: "black",
  },
}));

export { Instance_style, Grid_style, Form_style };
