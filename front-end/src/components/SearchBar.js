import React from "react";
import { Button, FormControl } from "@material-ui/core";
import { Form_style } from "./Styles";

function SearchBar(props) {
  const { setSearch, modelType } = props;
  const classes = Form_style();
  var placeholder = "Search " + modelType;
  var searchTemp = placeholder;
  return (
    <>
      <FormControl className={classes.formControl}>
        <input
          className="form-control mr-sm-2"
          width="120"
          placeholder={placeholder}
          onChange={(e) => {
            searchTemp = e.target.value;
          }}
        />
      </FormControl>
      <FormControl className={classes.formControl}>
        <Button
          onClick={(e) => {
            e.preventDefault();
            setSearch(searchTemp);
          }}
          className={classes.button}
          type="submit"
        >
          Search
        </Button>
      </FormControl>
    </>
  );
}
export default SearchBar;
