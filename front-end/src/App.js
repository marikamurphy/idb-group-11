import React from "react";
import Navbar from "./components/Navbar";
import Disasters from "./pages/Disasters";
import About from "./pages/About";
import Programs from "./pages/Programs";
import Locations from "./pages/Locations";
import DisasterInstance from "./pages/DisasterInstance";
import ProgramInstance from "./pages/ProgramInstance";
import LocationInstance from "./pages/LocationInstance";
import Splash from "./pages/Splash";
import SearchPage from "./pages/SearchPage";
import Visualization from "./pages/Visualization";
import Favicon from "react-favicon";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import capD from "./assets/capD.png";
//for routing to different pages
function App() {
  return (
    <Router>
      <div>
        {/* <Favicon url={capD} /> */}
        <Navbar />
        <Switch>
          <Route path="/" exact component={Splash} />
          <Route path="/disasters" exact component={Disasters} />
          <Route path="/disasters/:id" component={DisasterInstance} />
          <Route path="/programs" exact component={Programs} />
          <Route path="/programs/:id" component={ProgramInstance} />
          <Route path="/counties" exact component={Locations} />
          <Route path="/counties/:id" component={LocationInstance} />
          <Route path="/about" exact component={About} />
          <Route path="/visualization" exact component={Visualization} />
          <Route path="/searchAll/:id" component={SearchPage} />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
