from sqlalchemy.orm import sessionmaker

from Constants import (
    COUNTY_POP_API_URL,
    LAT_LONG_API_URL,
    LAT_LONG_API_KEY,
    STATES,
)
from Models import County, engine

from collections import defaultdict
import json
import requests

session = sessionmaker(bind=engine)()

# Utility functions
def init_county_data():
    county_data = dict()
    response = requests.get(COUNTY_POP_API_URL)
    if response.status_code != 200:
        print(
            "Something went wrong fetching population data:",
            str(response.status_code),
        )
        return None
    populations = response.json()
    it = iter(populations)
    next(it)  # Need to skip over first row
    for pop in it:
        county_data[pop[0].upper()] = (int(pop[1]), int(pop[3]))
    return county_data


def fix_county(county, state):
    return county.replace(f", {state}", f", {STATES[state].upper()}")


def to_proper(county):
    words = county.split(" ")[:-2]
    for i, word in enumerate(words):
        words[i] = word[0] + word[1:].lower()
    return " ".join(words)


# Add county data to db
def scrape_county_data(disasters, projects):
    print("Starting county data scrape...")
    county_data = init_county_data()
    for state in STATES:
        for c in disasters.get(state, []):
            disaster = disasters[state].get(c, [])
            project = projects.get(state, dict()).get(c, [])
            population, county_code = county_data.get(
                fix_county(c, state), (0, 0)
            )
            # get lat long
            lat, lon = float("nan"), float("nan")
            response = requests.get(
                LAT_LONG_API_URL
                + f"?state={state}&county={to_proper(c)}"
                + f"&$$app_token={LAT_LONG_API_KEY}"
            )
            if response.status_code != 200:
                print(
                    "Error occurred while fetching lat-long data:",
                    str(response.status_code),
                )
            else:
                data = response.json()
                if data:
                    lat, lon = float(data[0]["latitude"]), float(
                        data[0]["longitude"]
                    )
            county = County(
                county_code=county_code,
                name=c,
                state=state,
                population=population,
                latitude=lat,
                longitude=lon,
                disasters=";".join(disaster),
                projects=";".join(project),
                image_url="",
            )
            session.add(county)
        session.commit()
        print("Finished scraping data for", state)


def main():
    with open("county_disaster_data.json", "r") as d, open(
        "county_projects_data.json", "r"
    ) as p:
        disasters, projects = json.load(d), json.load(p)
        scrape_county_data(disasters, projects)
    print("Done")


if __name__ == "__main__":
    main()
