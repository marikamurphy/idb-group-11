# Constants that may be needed for backend work

DISASTERS_API_URL = (
    "https://www.fema.gov/api/open/v2/DisasterDeclarationsSummaries"
)
PROGRAMS_API_URL = (
    "https://www.fema.gov/api/open/v2/HazardMitigationAssistanceProjects"
)
COUNTY_POP_API_URL = (
    "https://api.census.gov/data/2019/pep/population?get=N"
    + "AME,POP&for=county:*&in=state:*"
)
LAT_LONG_API_URL = (
    "https://data.healthcare.gov/resource/geocodes-usa-with-counties.json"
)

LAT_LONG_API_KEY = "YplxjudOqVu6RRxcMzyoTksUD"

DATABASE_URL = (
    "postgresql://postgres:disaster@disaster-db.c64n6t7jh2nw."
    + "us-east-2.rds.amazonaws.com/postgres"
)

MONTHS = {
    1: "January",
    2: "February",
    3: "March",
    4: "April",
    5: "May",
    6: "June",
    7: "July",
    8: "August",
    9: "September",
    10: "October",
    11: "November",
    12: "December",
}

SEASONS = {1: "Winter", 2: "Spring", 3: "Summer", 4: "Fall"}

STATES = {
    "AL": "Alabama",
    "AK": "Alaska",
    "AZ": "Arizona",
    "AR": "Arkansas",
    "CA": "California",
    "CO": "Colorado",
    "CT": "Connecticut",
    "DE": "Delaware",
    "FL": "Florida",
    "GA": "Georgia",
    "HI": "Hawaii",
    "ID": "Idaho",
    "IL": "Illinois",
    "IN": "Indiana",
    "IA": "Iowa",
    "KS": "Kansas",
    "KY": "Kentucky",
    "LA": "Louisiana",
    "ME": "Maine",
    "MD": "Maryland",
    "MA": "Massachusetts",
    "MI": "Michigan",
    "MN": "Minnesota",
    "MS": "Mississippi",
    "MO": "Missouri",
    "MT": "Montana",
    "NE": "Nebraska",
    "NV": "Nevada",
    "NH": "New Hampshire",
    "NJ": "New Jersey",
    "NM": "New Mexico",
    "NY": "New York",
    "NC": "North Carolina",
    "ND": "North Dakota",
    "OH": "Ohio",
    "OK": "Oklahoma",
    "OR": "Oregon",
    "PA": "Pennsylvania",
    "RI": "Rhode Island",
    "SC": "South Carolina",
    "SD": "South Dakota",
    "TN": "Tennessee",
    "TX": "Texas",
    "UT": "Utah",
    "VT": "Vermont",
    "VA": "Virigina",
    "WA": "Washington",
    "WV": "West Virginia",
    "WI": "Wisconsin",
    "WY": "Wyoming",
}

DISASTERS_AND_EFFECTS = {
    "Toxic Substances": "Death, disease, environmental-destruction",
    "Tsunami": (
        "Destruction, death, disease, environmental-destruction, flo"
        + "oding, pollution"
    ),
    "Biological": "Death, disease, starvation, environmental-destruction",
    "Freezing": "Destruction, environmental-destruction",
    "Tornado": "Destruction, death, environmental-destruction",
    "Fishing Losses": "Environmental-destruction, economic-disruptions",
    "Earthquake": "Destruction, death, environmental-destruction",
    "Severe Storm(s)": "Destruction, flooding",
    "Dam/Levee Break": (
        "Destruction, death, environmental-destruction," + " flooding"
    ),
    "Chemical": "Death, disease, environmental-destruction",
    "Typhoon": "Destruction, death, environmental-destruction, flooding",
    "Volcano": "Destruction, death, environmental-destruction, pollution",
    "Snow": "Destruction, disease, economic-disruptions",
    "Terrorist": "Death",
    "Coastal Storm": (
        "Destruction, environmental-destruction," + " flooding, pollution"
    ),
    "Drought": (
        "Death, starvation, environmental-destruction,"
        + " economic-disruptions"
    ),
    "Hurricane": (
        "Destruction, death, disease, environmental-destruction, f"
        + "looding, pollution"
    ),
    "Mud/Landslide": (
        "Destruction, death, environmental-destruction," + " pollution"
    ),
    "Human Cause": "",
    "Fire": (
        "Destruction, death, environmental-destruction,"
        + " economic-disruptions"
    ),
    "Flood": (
        "Destruction, disease, death, environmental-destruction, flood"
        + "ing, pollution"
    ),
    "Other": "",
    "Severe Ice Storm": (
        "Destruction, death, disease, environment-destructi"
        + "on, economic-disruptions"
    ),
}
