# Functions to create dictionaries for our models for the API

# capitalize the first letter of every word in a space-separated string
def to_proper(county):
    if not county:
        return county
    arr = county.split()
    for i, word in enumerate(arr[:-1]):
        arr[i] = word[0] + word[1:].lower()
    return " ".join(arr)


# create a dict object from a query representing a disaster instance
def create_disaster_dict(query):
    effects = query.effects.split(", ")
    counties = [c for c in map(to_proper, query.location.split(";"))]
    disaster = {
        "id": query.disaster_id,
        "name": query.name,
        "fema_id": query.fema_id,
        "disaster_type": query.disaster_type,
        "date": query.date,
        "season": query.season,
        "effects": effects,
        "cost": query.cost,
        "image_url": query.image_url,
        "video_url": query.video_url,
        "counties": counties,
        "programs": query.projects.split(";")[1:],
        "county_image_urls": query.county_image_url.split("^"),
        "program_image_urls": query.program_image_url.split("^"),
    }
    return disaster


# create a dict object from a query representing a county instance
def create_county_dict(query):
    county = {
        "id": query.county_id,
        "name": to_proper(query.name),
        "code": query.county_code,
        "state": query.state,
        "population": query.population,
        "latitude": str(query.latitude),
        "longitude": str(query.longitude),
        "twitter_url": query.twitter_url,
        "disasters": [
            {"name": disaster} for disaster in query.disasters.split(";")
        ],
        "programs": query.projects.split(";"),
        "disaster_ids": query.disaster_id.split(";"),
        "image_url": query.image_url,
        "disaster_image_urls": query.disaster_image_url.split("^"),
        "program_image_urls": query.program_image_url.split("^"),
    }
    return county


# create a dict object from a query representing a program instance
def create_program_dict(query):
    counties = [c for c in map(to_proper, query.counties.split(";"))]
    program = {
        "id": query.program_id,
        "name": query.name,
        "type": query.project_type,
        "budget": query.amount,
        "project_id": query.project_id,
        "image_url": query.image_url,
        "counties": counties,
        "disaster": {"fema_id": query.disaster_id, "name": query.disaster},
        "disaster_image_urls": query.disaster_image_url.split("^"),
        "county_image_urls": query.county_image_url.split("^"),
    }
    return program
