from sqlalchemy.orm import sessionmaker

from Constants import PROGRAMS_API_URL, STATES, DISASTERS_AND_EFFECTS
from Models import Disaster, Program, engine

from collections import defaultdict
import json
import requests
from image_scraper import fetch_image_url

session = sessionmaker(bind=engine)()

# Add video and image url data to db
def scrape_programs_data():
    programs = session.query(Program)
    print("Starting programs data scrape...")

    for prog in programs:
        if prog.image_url == "" or prog.image_url == None:
            temp = session.query(Program).get(prog.program_id)
            temp.image_url = fetch_image_url(prog.name)
            session.commit()

    return programs


def main():
    scrape_programs_data()
    print("Done!")


if __name__ == "__main__":
    main()
