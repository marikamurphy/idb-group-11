from selenium import webdriver
import time
from selenium.webdriver.chrome.options import Options

options = Options()
options.add_argument("--headless")
# see https://towardsdatascience.com/image-scraping-with-python-a96feda8af2d
# Put the path for your ChromeDriver here
DRIVER_PATH = "/home/marika/cs/softeng/chromedriver_linux64/chromedriver"
wd = webdriver.Chrome(executable_path=DRIVER_PATH, options=options)


def fetch_image_urls(query, max_links_to_fetch=1, sleep_between_interactions=1):
    def scroll_to_end(wd):
        wd.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        time.sleep(sleep_between_interactions)

    # build the google query
    search_url = (
        "https://www.google.com/search?safe=off&site=&tbm=isch&sou"
        + "rce=hp&q={q}&oq={q}&gs_l=img"
    )

    # load the page
    wd.get(search_url.format(q=query))

    image_urls = set()
    image_count = 0
    results_start = 0
    while image_count < max_links_to_fetch:
        scroll_to_end(wd)

        # get all image thumbnail results
        thumbnail_results = wd.find_elements_by_css_selector("img.Q4LuWd")
        number_results = len(thumbnail_results)

        for img in thumbnail_results[results_start:number_results]:
            # try to click every thumbnail such that we can
            # get the real image behind it
            try:
                img.click()
                time.sleep(sleep_between_interactions)
            except Exception:
                continue

            # extract image urls
            actual_images = wd.find_elements_by_css_selector("img.n3VNCb")
            for actual_image in actual_images:
                if actual_image.get_attribute(
                    "src"
                ) and "http" in actual_image.get_attribute("src"):
                    image_urls.add(actual_image.get_attribute("src"))

            image_count = len(image_urls)

            if len(image_urls) >= max_links_to_fetch:
                print(f"Found: {len(image_urls)} image links, done!")
                break
        else:
            print(
                "Found:", len(image_urls), "image links, looking for more ..."
            )
            time.sleep(30)
            return
            load_more_button = wd.find_element_by_css_selector(".mye4qd")
            if load_more_button:
                wd.execute_script("document.querySelector('.mye4qd').click();")

        # move the result startpoint further down
        results_start = len(thumbnail_results)

    return image_urls


def fetch_image_url(query: str, sleep_between_interactions: int = 1):
    def scroll_to_end(wd):
        wd.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        time.sleep(sleep_between_interactions)

    # build the google query
    search_url = (
        "https://www.google.com/search?safe=off&site=&tbm=isch&sou"
        + "rce=hp&q={q}&oq={q}&gs_l=img"
    )

    # load the page
    wd.get(search_url.format(q=query))
    # get all image thumbnail results
    thumbnail_results = wd.find_elements_by_css_selector("img.Q4LuWd")
    number_results = len(thumbnail_results)

    for img in thumbnail_results[0:number_results]:
        # try to click every thumbnail such that we can
        # get the real image behind it
        try:
            img.click()
            time.sleep(sleep_between_interactions)
        except Exception:
            continue
        # extract image urls
        actual_images = wd.find_elements_by_css_selector("img.n3VNCb")
        for actual_image in actual_images:
            if actual_image.get_attribute(
                "src"
            ) and "http" in actual_image.get_attribute("src"):
                return actual_image.get_attribute("src")
