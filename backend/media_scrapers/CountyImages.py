from sqlalchemy.orm import sessionmaker

from Constants import STATES, DISASTERS_AND_EFFECTS
from Models import County, Program, engine

from collections import defaultdict
import json
import requests
from image_scraper import fetch_image_url

session = sessionmaker(bind=engine)()

# Add video and image url data to db
def scrape_county_data():
    counties = session.query(County)
    print("Starting programs data scrape...")

    for county in counties:
        if county.image_url == "" or county.image_url == None:
            temp = session.query(County).get(county.county_id)
            temp.image_url = fetch_image_url(county.name+" "+county.state)
            session.commit()

    return counties


def main():
    scrape_county_data()
    print("Done!")


if __name__ == "__main__":
    main()
