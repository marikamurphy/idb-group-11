import time
from googlesearch import search as gsearch
from random import randint, random

from sqlalchemy.orm import sessionmaker

from Models import County, engine

from collections import defaultdict
import json
import requests

session = sessionmaker(bind=engine)()


def scrape_lists_with_google(keyword_string):
    keyword_string = "weather " + keyword_string
    urls_checked = 0

    # Perform the Google search, checking only the twitter.com domain
    for url in gsearch(
        "site:twitter.com " + keyword_string,
        start=urls_checked,
        num=10,
        pause=randint(3, 5),
    ):
        return process_url(url)


def process_url(url):
    new_url = url + "?ref_src=twsrc%5Etfw"
    return new_url


# Add embeddable twitter feed url to database for counties
def scrape_county_data():
    counties = session.query(County)
    print("Starting programs data scrape...")

    for county in counties:
        if county.twitter_url == "" or county.twitter_url == None:
            temp = session.query(County).get(county.county_id)
            temp.twitter_url = scrape_lists_with_google(county.name)
            session.commit()

    # return counties


def main():
    scrape_county_data()
    print("Done!")


if __name__ == "__main__":
    main()
