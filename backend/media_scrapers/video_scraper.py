from selenium import webdriver
import time
from selenium.webdriver.chrome.options import Options

options = Options()
options.add_argument("--headless")
# see https://towardsdatascience.com/image-scraping-with-python-a96feda8af2d
# Put the path for your ChromeDriver here
DRIVER_PATH = "/home/marika/cs/softeng/chromedriver_linux64/chromedriver"
wd = webdriver.Chrome(executable_path=DRIVER_PATH, options=options)


def fetch_video_url(query: str, sleep_between_interactions: int = 1):
    def scroll_to_end(wd):
        wd.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        time.sleep(sleep_between_interactions)

    # build the google query
    search_url = "https://www.youtube.com/results?search_query={q}"

    # load the page
    wd.get(search_url.format(q=query))
    # extract video urls
    videos = wd.find_elements_by_css_selector("#video-title")
    for video in videos:
        if video.get_attribute("href"):
            return process_url(video.get_attribute("href"))

    return "Error"


def process_url(url):
    first, second = url.split("=", 2)
    new_url = "https://www.youtube.com/embed/" + second
    return new_url
