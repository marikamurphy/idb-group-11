from sqlalchemy.orm import sessionmaker

from Constants import PROGRAMS_API_URL, STATES, DISASTERS_AND_EFFECTS
from Models import Disaster, Program, engine

from collections import defaultdict
import json
import requests
from image_scraper import fetch_image_url

session = sessionmaker(bind=engine)()

# Add video and image url data to db
def scrape_programs_data():
    disasters = session.query(Disaster)
    print("Starting programs data scrape...")

    for disaster in disasters:
        if disaster.image_url == "" or disaster.image_url == None:
            temp = session.query(Disaster).get(disaster.disaster_id)
            temp.image_url = fetch_image_url(disaster.name)
            session.commit()

    return disasters


def main():
    scrape_programs_data()
    print("Done!")


if __name__ == "__main__":
    main()
