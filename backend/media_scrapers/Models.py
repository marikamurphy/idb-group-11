from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine, Column, Float, Integer, String

from Constants import DATABASE_URL

Base = declarative_base()
engine = create_engine(DATABASE_URL, echo=False)


class Disaster(Base):
    __tablename__ = "disaster"

    disaster_id = Column(Integer, primary_key=True)
    fema_id = Column(Integer)
    name = Column(String)
    location = Column(String)
    disaster_type = Column(String)
    date = Column(String)
    season = Column(String)
    effects = Column(String)
    cost = Column(Float)
    projects = Column(String)
    image_url = Column(String)
    video_url = Column(String)


class County(Base):
    __tablename__ = "county"

    county_id = Column(Integer, primary_key=True)
    county_code = Column(Integer)
    name = Column(String)
    state = Column(String)
    population = Column(Integer)
    latitude = Column(Float)
    longitude = Column(Float)
    disasters = Column(String)
    projects = Column(String)
    twitter_url = Column(String)
    image_url = Column(String)


class Program(Base):
    __tablename__ = "program"

    program_id = Column(Integer, primary_key=True)
    name = Column(String)
    project_type = Column(String)
    amount = Column(Integer)
    project_id = Column(String)
    counties = Column(String)
    disaster = Column(String)
    image_url = Column(String)


# uncomment and run this file to create the tables
# Base.metadata.create_all(engine, checkfirst=True)

# uncomment and run this file to delete the tables.
# We have scripts to scrape data, so if tables deleted by accident,
# we can recover the data easily
# Disaster.__table__.drop(engine)
# County.__table__.drop(engine)
# Program.__table__.drop(engine)
