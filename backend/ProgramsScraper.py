from sqlalchemy.orm import sessionmaker

from Constants import PROGRAMS_API_URL, STATES, DISASTERS_AND_EFFECTS
from Models import Disaster, Program, engine

from collections import defaultdict
import json
import requests

session = sessionmaker(bind=engine)()

# Utility functions
def invert_dict(d):
    return {v: k for k, v in d.items()}


def get_counties(counties, state):
    return [
        f"{county} COUNTY, {state.upper()}"
        for county in counties
        if county != "Statewide" and "Indian Reservation" not in county
    ]


# Add programs data to db and return county data for county data scraping
def scrape_programs_data():
    county_data = defaultdict(lambda: defaultdict(list))
    inv_states = invert_dict(STATES)
    print("Starting programs data scrape...")
    for state in inv_states:
        skip = 0
        while True:
            response = requests.get(
                PROGRAMS_API_URL + f"?$filter=state eq '{state}'&$skip={skip}"
            )
            if response.status_code != 200:
                print(
                    "Response return status code: " + str(response.status_code)
                )
                break
            projects = response.json()["HazardMitigationAssistanceProjects"]
            if len(projects) == 0:
                break
            for p in projects:
                disaster_name = (
                    session.query(Disaster.name)
                    .filter_by(fema_id=p["disasterNumber"])
                    .first()
                )  # This assumes that there's data in the disaster table
                if disaster_name == None:
                    continue
                disaster_name = disaster_name[0]
                session.query(Disaster).filter_by(
                    fema_id=p["disasterNumber"]
                ).update({Disaster.cost: p["projectAmount"]})
                counties = get_counties(
                    p["projectCounties"].split("; "), inv_states[state]
                )
                program = Program(
                    name=p["projectTitle"],
                    project_type=p["projectType"],
                    amount=p["projectAmount"],
                    project_id=p["projectIdentifier"],
                    counties=";".join(counties),
                    disaster=disaster_name,
                    image_url="",
                )
                session.add(program)
                session.commit()
                # put county data in json
                for county in counties:
                    county_data[inv_states[state]][county].append(
                        p["projectTitle"]
                    )
            skip += len(projects)
        print("Finished scraping data for", state)
    return county_data


def main():
    county_data = scrape_programs_data()
    with open("county_projects_data.json", "w") as f:
        json.dump(county_data, f)
    print("Done!")


if __name__ == "__main__":
    main()
