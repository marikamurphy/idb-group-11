import json
import requests
from unittest import main, TestCase
import api
from api import app
from Models import County
from dictionary import to_proper


class TestBackend(TestCase):

    # test transforming county name to proper format
    def test_to_proper(self):
        county = to_proper("TraviS CoUnTY, TX")
        self.assertEqual(county, "Travis County, TX")

    # test getting the state from location
    def test_get_state(self):
        state = api.get_state("Travis County, TX")
        self.assertEqual(state, "TX")

    # test filtering out nonsense parameters
    def test_get_filter_dict(self):
        res = {"name": "Travis"}
        nonsense = {"name": "Travis", "nonsense": "abracadabra"}
        filter_dict = api.get_filter_dict(County, nonsense)
        self.assertEqual(filter_dict, res)

    # test / endpoint
    def test_index(self):
        test = app.test_client(self)
        r = test.get("/")
        self.assertEqual(r.status_code, 200)
        self.assertEqual(r.data.decode("utf8")[0:9], "<!doctype")

    # test basic endpoints available
    def test_api(self):
        test = app.test_client(self)
        r = test.get("/api/disasters")
        self.assertEqual(r.status_code, 200)
        r = test.get("/api/counties")
        self.assertEqual(r.status_code, 200)
        r = test.get("/api/programs")
        self.assertEqual(r.status_code, 200)

    # test filtering/response format for programs
    def test_programs(self):
        res = {
            "Program": {
                "budget":28990,
                "counties":["St. Clair County, AL"],
                "county_image_urls":["https://upload.wikimedia.org/wikipedia/commons/thumb/1/10/Map_of_Alabama_highlighting_St_Clair_County.svg/150px-Map_of_Alabama_highlighting_St_Clair_County.svg.png"],
                "disaster":
                    {
                        "fema_id":1971,
                        "name":"SEVERE STORMS, TORNADOES, STRAIGHT-LINE WINDS, AND FLOODING April 15, 2011 AL"
                    },
                "disaster_image_urls":["https://www.weather.gov/images/mob/events/2011_April15_Tornado/2011April15_outbreak2.png"],
                "id":244,
                "image_url":"https://s3media.247sports.com/Uploads/Assets/590/513/9513590.jpg?fit=bounds&crop=1200:630,offset-y0.50&width=1200&height=630",
                "name":"City of Moody RSR 7",
                "project_id":"DR-1971-0216-R",
                "type":"206.1: Safe Room (Tornado and Severe Wind Shelter) - Private Structures"
            }
        }

        test = app.test_client(self)
        r = test.get("/api/programs?name=City%20of%20Moody%20RSR%207")
        self.assertEqual(r.status_code, 200)
        data = json.loads(r.data.decode("utf8"))
        
        self.assertEqual(data[0], res)

    # test filtering for counties: no result
    def test_counties_no_res(self):
        test = app.test_client(self)
        r = test.get("/api/counties?state=NA")
        self.assertEqual(r.status_code, 200)
        data = json.loads(r.data.decode("utf8"))
        self.assertEqual(data, [0])

    # test filtering for counties
    def test_counties_filtering(self):
        test = app.test_client(self)
        r = test.get("/api/counties?state=AZ")
        self.assertEqual(r.status_code, 200)
        data = json.loads(r.data.decode("utf8"))
        self.assertEqual(len(data), 11) #pagination

    # test filtering for disasters with multiple params
    def test_disasters_multi_params(self):
        test = app.test_client(self)
        r = test.get("/api/disasters?cost=25000&season=Spring")
        self.assertEqual(r.status_code, 200)
        data = json.loads(r.data.decode("utf8"))
        self.assertEqual(len(data), 2)


if __name__ == "__main__":
    main()
