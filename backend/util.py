from sqlalchemy import cast, Date

# -----------------
# Utility Functions
# -----------------

# get the state abbreviation from a county string
def get_state(county):
    return county.split(" ")[-1]


# get a dict of valid arguments that can be used to query a model
def get_filter_dict(model, args):
    return {attr: args[attr] for attr in model.__dict__ if attr in args}


# check if a string can represent a number
def is_int(num):
    try:
        int(num)
        return True
    except:
        return False


# get the limit and offset for pagination in args dict
def get_limit_offset(args):
    limit = args.get("limit", 10)
    offset = args.get("offset", 0)
    if not is_int(limit):
        limit = 10
    if not is_int(offset):
        offset = 0
    limit = int(limit)
    offset = int(offset)
    if limit < 0:
        limit = 10
    if offset < 0:
        limit = 0
    return limit + offset, offset


# return column to order the query for this model by
def get_order_criteria(model, criteria):
    if criteria == None:
        return None
    args = criteria.split()
    asc = len(args) < 2 or args[1] == "asc"
    col = None
    for column in model.__table__.columns:
        if column.name == args[0].lower():
            col = column
            break
    else:
        return None
    if col.name == "date":
        col = cast(col, Date)
    return col if asc else col.desc()
