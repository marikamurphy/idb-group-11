from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from Constants import (
    DISASTERS_API_URL,
    MONTHS,
    SEASONS,
    STATES,
    DISASTERS_AND_EFFECTS,
)
from Models import Disaster, engine

from collections import defaultdict
from dateutil.parser import isoparse
import json
import requests

session = sessionmaker(bind=engine)()

# Utility Functions
def convert_to_date(date):
    iso_date = isoparse(date)
    return f"{MONTHS[iso_date.month]} {iso_date.day}, {iso_date.year}"


def convert_to_county(county, state):
    arr = county.split(" ")[:-1]
    return f'{" ".join(arr)} County, {state}'


def get_disaster_name(name, disaster_type, date, state):
    if disaster_type in (
        "Freezing",
        "Tornado",
        "Earthquake",
        "Severe Storm(s)",
        "Snow",
        "Drought",
        "Mud/Landslide",
        "Flood",
        "Severe Ice Storm",
    ):
        return f"{name} {convert_to_date(date)} {state}"
    return name


def create_disaster(fema_id, name, counties, disaster_type, date, effects):
    str_date = convert_to_date(date)
    season = get_season(date)
    disaster = Disaster(
        fema_id=fema_id,
        name=name,
        location=";".join(counties),
        disaster_type=disaster_type,
        date=str_date,
        season=season,
        effects=effects,
        cost=0,
        projects="",
        image_url="",
        video_url="",
    )
    return disaster


def get_season(date):
    date = isoparse(date)
    return SEASONS[(date.month % 12 + 3) // 3]


# Add disaster data to db and return county data for county data scraping
def scrape_disaster_data():
    county_data = defaultdict(lambda: defaultdict(list))
    print("Starting disaster data scrape...")
    for state in STATES:
        skip = 0
        counties = None
        disaster_num = -1
        name, disaster_type, date, effects = None, None, None, None
        while True:
            response = requests.get(
                DISASTERS_API_URL
                + f"?$skip={skip}&$filter=state eq '{state}'"
                + "&$orderby=disasterNumber"
            )
            if response.status_code != 200:
                print(
                    "Response return status code: " + str(response.status_code)
                )
                break
            disasters = response.json()["DisasterDeclarationsSummaries"]
            if len(disasters) == 0:
                disaster = create_disaster(
                    disaster_num, name, counties, disaster_type, date, effects
                )
                session.add(disaster)
                session.commit()
                break
            # One disaster instance per disaster_num.
            # One-to-many relationship between disaster numbers and counties
            for d in disasters:
                if disaster_num != d["disasterNumber"]:
                    if disaster_num != -1 and len(counties) > 0:
                        disaster = create_disaster(
                            disaster_num,
                            name,
                            counties,
                            disaster_type,
                            date,
                            effects,
                        )
                        session.add(disaster)
                        session.commit()
                    counties = []
                    disaster_num = d["disasterNumber"]
                    date, disaster_type = (
                        d["incidentBeginDate"],
                        d["incidentType"],
                    )
                    effects = DISASTERS_AND_EFFECTS[d["incidentType"]]
                    name = get_disaster_name(
                        d["declarationTitle"], disaster_type, date, state
                    )
                if "(County)" in d["designatedArea"]:
                    county = convert_to_county(
                        d["designatedArea"], state
                    ).upper()
                    counties.append(county)
                    county_data[state][county].append(name)
            skip += len(disasters)
        print("Finished scraping data for", state)
    return county_data


def main():
    county_data = scrape_disaster_data()
    with open("county_disaster_data.json", "w") as f:
        json.dump(county_data, f)
    print("Done")


if __name__ == "__main__":
    main()
