from sqlalchemy.orm import sessionmaker
from sqlalchemy.inspection import inspect
from sqlalchemy import cast, Date, String, func, distinct

from flask import Flask, request, json, jsonify, render_template
from flask_cors import CORS

from Constants import STATES
from Models import County, Disaster, Program, engine
from dictionary import (
    create_disaster_dict,
    create_county_dict,
    create_program_dict,
)
from util import (
    get_state,
    get_filter_dict,
    is_int,
    get_limit_offset,
    get_order_criteria,
)

from collections import defaultdict
import json
import re
import requests

# bind db session to our db
session = sessionmaker(bind=engine)()

# -----------------
# Utility Functions
# -----------------

# create and configure Flask server
def create_app():
    app = Flask(
        __name__,
        static_folder="../front-end/build/static",
        template_folder="../front-end/build",
    )
    CORS(app)
    app.config["CORS_HEADERS"] = "Content-Type"
    return app


# used for getting name of model and its dict creation
# function using the model's class.
MODEL_DICT = {
    Disaster: ("Disaster", create_disaster_dict),
    Program: ("Program", create_program_dict),
    County: ("County", create_county_dict),
}

# used to get which columns to ignore based on the model
IGNORE_COLS = {
    Disaster: (
        "disaster_id",
        "location",
        "effects",
        "projects",
        "image_url",
        "video_url",
    ),
    Program: (
        "program_id",
        "project_id",
        "counties",
        "image_url",
        "disaster_id",
    ),
    County: (
        "county_id",
        "county_code",
        "latitude",
        "longitude",
        "disasters",
        "projects",
        "twitter_url",
        "disaster_id",
    ),
}


# get all instances of a certain model that match terms from a search query
def search_models(model, search):
    terms = re.sub(" +", " ", search).split()
    try:
        query = session.query(model)
        rows = set(
            range(1, len(query.all()) + 1)
        )  # getting rows by id. start with all rows
        pk = inspect(model).primary_key[0]
        for term in terms:
            # Here we want to winnow down the query based on the terms
            # Check for matching terms in every column
            temp_query = session.query(model).filter(False)
            for column in model.__table__.columns:
                # don't want to match based on these columns
                if column.name in IGNORE_COLS.get(model, ()):
                    continue
                if str(column.type) in ("INTEGER", "FLOAT"):
                    # trying to match number-based columns
                    temp_query = temp_query.union(
                        query.filter(
                            cast(column, String).ilike("%" + term + "%")
                        )
                    )
                elif str(column.type) == "VARCHAR":
                    temp_query = temp_query.union(
                        query.filter(column.ilike("%" + term + "%"))
                    )
            temp_rows = set([getattr(q, pk.name) for q in temp_query.all()])
            rows = rows.intersection(temp_rows)  # winnow
    except Exception as e:
        raise
    return session.query(model).filter(pk.in_(rows))


# -------------
# API endpoints
# -------------
app = create_app()

# homepage
@app.route("/", defaults={"path": ""})
@app.route("/<path:path>")
def index(path):
    return render_template("index.html")


# disasters endpoint
@app.route("/api/disasters", methods=["GET"])
def get_disasters():
    search = request.args.get("search", None)
    # get limit and offset for pagination
    limit, offset = get_limit_offset(request.args)
    order = request.args.get("order", None)
    queries = (
        search_models(Disaster, search) if search else session.query(Disaster)
    )
    try:
        # if search arg is only arg present, search model
        filt_dict = get_filter_dict(Disaster, request.args)
        queries = queries.filter_by(**filt_dict)
        disasters = [
            {"Disaster": create_disaster_dict(q)}
            for q in queries.order_by(get_order_criteria(Disaster, order))
            .slice(offset, limit)
            .all()
        ] + [queries.count()]
    except Exception as e:
        print(e)
        return "Error retrieving your request. Try again."
    return jsonify(disasters)


# disasters id endpoint
@app.route("/api/disasters/<id>", methods=["GET"])
def get_disaster_by_id(id):
    query = session.query(Disaster).filter_by(fema_id=id).first()
    if not query:
        return []
    disaster = create_disaster_dict(query)
    return jsonify([disaster])


# counties endpoint
@app.route("/api/counties", methods=["GET"])
def get_counties():
    search = request.args.get("search", None)
    # get limit and offset for pagination
    limit, offset = get_limit_offset(request.args)
    order = request.args.get("order", None)
    queries = (
        search_models(County, search) if search else session.query(County)
    )
    try:
        # if search arg is only arg present, search model
        filt_dict = get_filter_dict(County, request.args)
        queries = queries.filter_by(**filt_dict)
        counties = [
            {"County": create_county_dict(q)}
            for q in queries.order_by(get_order_criteria(County, order))
            .slice(offset, limit)
            .all()
        ] + [queries.count()]
    except Exception as e:
        print(e)
        return "Error retrieving your request. Try again."
    return jsonify(counties)


# counties id api endpoint
@app.route("/api/counties/<id>", methods=["GET"])
def get_county_by_id(id):
    query = session.query(County).filter_by(county_id=id).first()
    if not query:
        return []
    county = create_county_dict(query)
    return jsonify([county])


# programs endpoint
@app.route("/api/programs", methods=["GET"])
def get_programs():
    search = request.args.get("search", None)
    # get limit and offset for pagination
    limit, offset = get_limit_offset(request.args)
    order = request.args.get("order", None)
    queries = (
        search_models(Program, search) if search else session.query(Program)
    )
    try:
        # filter query based on args give from api call
        filt_dict = get_filter_dict(Program, request.args)
        queries = queries.filter_by(**filt_dict)
        programs = [
            {"Program": create_program_dict(q)}
            for q in queries.order_by(get_order_criteria(Program, order))
            .slice(offset, limit)
            .all()
        ] + [queries.count()]
    except Exception as e:
        print(e)
        return "Error retrieving your request. Try again."
    return jsonify(programs)


# programs id endpoint
@app.route("/api/programs/<id>", methods=["GET"])
def get_program_by_id(id):
    query = session.query(Program).filter_by(program_id=id).first()
    if not query:
        return []
    program = create_program_dict(query)
    return jsonify([program])


# endpoint specifically for searching whole website, not for api clients
@app.route("/api/search/<search_string>", methods=["GET"])
def search_all_models(search_string):
    results_dict = {}
    try:
        # get search results for each of the three models
        for model, (name, create_dict) in MODEL_DICT.items():
            query = search_models(model, search_string)
            if name == "County":
                name = "Countie"
            results_dict[f"{name}s"] = [
                {name: create_dict(q)} for q in query
            ] + [query.count()]
    except:
        print(e)
        return "Error retrieving your request. Try again."
    return jsonify(results_dict)


# -----------------------
# Visualization Endpoints
# -----------------------


@app.route("/api/disasters/visual", methods=["GET"])
def get_disasters_visual():
    data = session.query(Disaster.date)
    dates = defaultdict(int)
    for d in data:
        dates[d[0][-4:]] += 1
    return jsonify(dates)


@app.route("/api/programs/visual", methods=["GET"])
def get_programs_visual():
    programs_per_state = defaultdict(int)
    budget_per_state = defaultdict(int)
    data = session.query(Program.counties, Program.amount)
    for d in data:
        state = get_state(d.counties.split(";")[0])
        programs_per_state[STATES[state]] += 1
        budget_per_state[STATES[state]] += d.amount if d.amount != None else 0
    stats = dict()
    stats["programs_per_state"] = programs_per_state
    stats["budget_per_state"] = budget_per_state
    return jsonify(stats)


@app.route("/api/counties/visual", methods=["GET"])
def get_counties_visual():
    counties = [
        {
            "name": q.name,
            "coordinates": [q.longitude, q.latitude],
            "population": q.population,
        }
        for q in session.query(County)
    ]
    return jsonify(counties)


# run flask server
if __name__ == "__main__":
    app.run(host="0.0.0.0", port=80, debug=True)
