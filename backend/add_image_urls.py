from sqlalchemy.orm import sessionmaker

from Constants import PROGRAMS_API_URL, STATES, DISASTERS_AND_EFFECTS
from Models import County, Disaster, Program, engine

from collections import defaultdict
import json
import requests

session = sessionmaker(bind=engine)()


def get_all_urls():
    disaster_urls = {
        q.fema_id: q.image_url
        for q in session.query(Disaster.fema_id, Disaster.image_url)
    }
    county_urls = {
        q.name: q.image_url
        for q in session.query(County.name, County.image_url)
    }
    program_urls = {
        q.name: q.image_url
        for q in session.query(Program.name, Program.image_url)
    }
    return disaster_urls, county_urls, program_urls


def update_disasters(counties, programs):
    print("Updating Disasters")
    for q in session.query(Disaster).all():
        print("d", q.disaster_id)
        county_urls = [
            (counties.get(county, "") or "")
            for county in q.location.split(";")
        ]
        program_urls = [
            (programs.get(program, "") or "")
            for program in q.projects.split(";")
        ]
        session.query(Disaster).filter_by(disaster_id=q.disaster_id).update(
            {
                Disaster.county_image_url: "^".join(county_urls),
                Disaster.program_image_url: "^".join(program_urls),
            }
        )
    print("Done updating Disaster Table")


def update_counties(disasters, programs):
    print("Updating Counties")
    for q in session.query(County):
        print("c", q.county_id)
        disaster_urls = [
            disasters.get(int(disaster), "") or ""
            for disaster in q.disaster_id.split(";")
        ]
        program_urls = [
            programs.get(program, "") or ""
            for program in q.projects.split(";")
        ]
        session.query(County).filter_by(county_id=q.county_id).update(
            {
                County.disaster_image_url: "^".join(disaster_urls),
                County.program_image_url: "^".join(program_urls),
            }
        )
    print("Done updating County Table")


def update_programs(disasters, counties):
    print("Updating Programs")
    for q in session.query(Program):
        print("p", q.program_id)
        disaster_urls = [disasters.get(q.disaster_id, "") or ""]
        county_urls = [
            counties.get(county, "") or "" for county in q.counties.split(";")
        ]
        session.query(Program).filter_by(program_id=q.program_id).update(
            {
                Program.disaster_image_url: "^".join(disaster_urls),
                Program.county_image_url: "^".join(county_urls),
            }
        )
    print("Done updating Program Table")


def update_tables():
    disasters, counties, programs = get_all_urls()
    update_disasters(counties, programs)
    update_counties(disasters, programs)
    update_programs(disasters, counties)
    session.commit()
    print("Table update complete")


def main():
    update_tables()
    print("Done")


if __name__ == "__main__":
    main()
