ifeq ($(shell uname), Darwin)          # Apple
	PYTHON   := python3
	PIP      := pip3
	BLACK         := black
else ifeq ($(shell uname -p), unknown) # Windows
	PYTHON   := python                 
	PIP      := pip3
	BLACK         := black
else                                   # UTCS
	PYTHON   := python3
	PIP      := pip3
	BLACK         := black
endif


clean:
	rm -f  *.pyc
	rm -rf __pycache__

format:
	$(BLACK) src/backend

