# IDB Group 11

A cross-platform, responsive, Web app that emulates IMDB to provide useful information about natural disasters.

name: Marika Murphy  
EID:  mmm7782  
GitLab ID:  marikamurphy
estimated completion time(hours: int):  20   
actual completion time(hours: int): 20  

name: Brandon Nsidinanya  
EID:  bnn393  
GitLab ID:  Brandon0329  
estimated completion time(hours: int):  15  
actual completion time(hours: int):   20  

name: Zongying Mo  
EID:zm3998  
GitLab ID:zmo617  
estimated completion time(hours: int):  20  
actual completion time(hours: int): 20  

Git SHA: f113c894120db89e7ea4af250c3d51243d713ad0  
project leader: all members.  
link to GitLab pipelines: https://gitlab.com/marikamurphy/idb-group-11/-/pipelines  
link to website: https://disasterinfo.me  

comments: npm package "react-favicon" does not work.
