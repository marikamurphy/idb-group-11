#!/bin/bash
# a simple script to update our website whenever we push to Gitlab
# this script should be run as a job in gitlab-ci.yml file

EC2=ec2-user@ec2-18-220-39-200.us-east-2.compute.amazonaws.com

cat swe-group-11.pem

ssh -i 'swe-group-11.pem' $EC2 /bin/bash << EOF
    cd idb-group-11/
    sudo git pull
    sudo docker ps
    sudo docker container restart a3e01effaafd
    echo "Website Updated"
EOF
